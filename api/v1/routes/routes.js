module.exports = function (express) {
  var router = express.Router();

  //__________________________________
  router.use('/user', require('../controller/user.controller')(express));
  // Examiner Routes
  router.use('/examiner/test', require('../controller/examiner/test.controller')(express));
  router.use('/examiner/test-v2', require('../controller/examiner/test-v2.controller')(express));
  router.use('/examiner/exam', require('../controller/examiner/exam.controller')(express));
  router.use('/examiner/examinee', require('../controller/examiner/examinee.controller')(express));
  router.use('/examiner/exam-offer', require('../controller/examiner/exam-offer.controller')(express));
  // Examinee Routes
  router.use('/examinee/exam', require('../controller/examinee/exam.controller')(express));
  router.use('/examinee/exam-offer', require('../controller/examinee/exam-offer.controller')(express));
  router.use('/examinee/draft', require('../controller/examinee/draft.controller')(express));
  router.use('/examinee/exam-process', require('../controller/examinee/exam-process.controller')(express));
  
  return router;
};
