module.exports = function (express) {
  let router = express.Router();
  let ExamModel = require('../../model/exam.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let mongoose = require('mongoose');

  router.post('/', async(req, res) => {
    if (!req.body) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'Exam data should be provided.');
    }

    if (!req.body['test'] || (!mongoose.Types.ObjectId.isValid(req.body['test']) && !mongoose.Types.ObjectId.isValid(req.body.test['_id']) ) ) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You should provide VALID test ID.');
    }

    let exam = new ExamModel(req.body);
    try {
      let savedExam = await exam.save();
      return res.status(200).json(savedExam);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }
  });

  router.put('/', async(req, res) => {
    if (!req.body || !req.body['_id'] || !mongoose.Types.ObjectId.isValid(req.body['_id'])) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'Exam data should be provided.');
    }

    if (!req.body['test'] || (!mongoose.Types.ObjectId.isValid(req.body['test']) && !mongoose.Types.ObjectId.isValid(req.body.test['_id']) ) ) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You should provide VALID test ID.');
    }


    try {
      let updatedExam = await ExamModel.findByIdAndUpdate(req.body['_id'], req.body, {new: true});
      return res.status(200).json(updatedExam);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }
  });

  router.delete('/:examId', async (req, res) => {
    let examId = req.params.examId;
    if (!examId || !mongoose.Types.ObjectId.isValid(examId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You must provide valid examId');
    }

    try {
      await ExamModel.findOneAndRemove({'_id': examId}).exec();
      return res.status(200).json({});
    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }
  });

  router.get('/owner/:examinerId', async (req, res) => {
    let examinerId = req.params.examinerId;
    if (!examinerId || !mongoose.Types.ObjectId.isValid(examinerId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You must provide valid examinerId');
    }

    let query = {
      $and: [{owner: examinerId}]
    };

    if (req.query && req.query.searchTerm) {
      let term = req.query.searchTerm;
      query['$and'].push({
        $or: [
          {title: term},
          {title: {$regex: new RegExp(`^${term}.*`, 'i')}},
          {title: {$regex: new RegExp(`.*${term}.*`, 'i')}},
          {title: {$regex: new RegExp(`.*${term}$`, 'i')}}
        ]
      })
    }

    try {
      let exams = await ExamModel.find(query).populate({path: 'offers'}).exec();
      return res.status(200).json({exams: exams});
    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }

  });


  return router;
}
