module.exports = function (express) {
  let router = express.Router();
  let TestModel = require('../../model/test.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let mongoose = require('mongoose');

  router.post('/', async (req, res) => {
    if (!req.body) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'Test data should be provided.');
    }

    let test = new TestModel(req.body);
    if (!test.partialStateQuestionQuantity) {
      delete test.partialStateQuestionQuantity;
    }
    try {
      let savedTest = await test.save();
      return res.status(200).json(savedTest);
    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }
  });

  router.put('/', async (req, res) => {
    if (!req.body || !req.body['_id']) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'Test data should be provided.');
    }

    if (!req.body.partialStateQuestionQuantity) {
      delete req.body.partialStateQuestionQuantity;
    }

    try {
      let updatedTest = await TestModel.findByIdAndUpdate(req.body['_id'], req.body, {new: true});
      return res.status(200).json(updatedTest);
    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }
  });

  router.get('/item/:testId', async(req, res) => {
    let testId = req.params.testId;
    if (!testId || !mongoose.Types.ObjectId.isValid(testId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You must provide valid testId');
    }

    try {
      let test = await TestModel.findById(testId).exec();
      return res.status(200).json(test);
    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }
  });

  router.get('/owner/:examinerId', async (req, res) => {
    let examinerId = req.params.examinerId;
    if (!examinerId || !mongoose.Types.ObjectId.isValid(examinerId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You must provide valid examinerId');
    }

    try {
      let tests = await TestModel.find({'owner': examinerId}).exec();
      return res.status(200).json({tests: tests});
    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }

  });

  return router;
}
