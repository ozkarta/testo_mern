module.exports = function (express) {
  let router = express.Router();
  let ExamModel = require('../../model/exam.model').model;
  let ExamOfferModel = require('../../model/exam-offer.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let mongoose = require('mongoose');

  router.post('/', async(req, res) => {
    if (!req.body) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'Exam Offer data should be provided.');
    }
    let validationFields = ['from', 'to', 'exam'];
    validationFields.forEach(field => {
      validateRequestBodyIDFields(field);
    });

    let examOffer = new ExamOfferModel(req.body);
    try {
      let savedExamOffer = await examOffer.save();
      await ExamModel.findByIdAndUpdate(savedExamOffer.exam, {$push:{'offers': savedExamOffer}}, {new: true});
      let updatedExam = await ExamModel.findById(savedExamOffer.exam)
        .populate({path: 'offers'})
        .exec();

      return res.status(200).json({offer: savedExamOffer, exam: updatedExam});
    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }


    function validateRequestBodyIDFields(field) {
      if (!req.body[field] || !mongoose.Types.ObjectId.isValid(req.body[field]) ) {
        return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, `You should provide VALID ${field}.`);
      }
    }
  })

  return router;
}
