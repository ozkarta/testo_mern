module.exports = function (express) {
  let router = express.Router();
  let UserModel = require('../../model/user.model').model;
  let jwt = require('jsonwebtoken');
  let bcrypt = require('bcryptjs');
  let config = require('../../../../config');
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let mongoose = require('mongoose');

  router.get('/', async (req, res) => {
    return res.status(200).json({ users: [] });
  });

  router.get('/in-relationship/:userId', async (req, res) => {
    let userId = req.params.userId;
    if (!userId || !mongoose.Types.ObjectId.isValid(userId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You should include VALID userId in request parameters.');
    }

    try {
      let user = await UserModel.findById(userId)
                  .populate({ path: 'relationship.user'})
                  .exec();
      if (!user) {
        return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'userId you provided does not exist.');
      }
      let result = user.relationship.map(relationship => {
        if (relationship.type === 'examinee') {
          delete relationship.user.passwordHash;
          return relationship.user;
        }
      });

      return res.status(200).json({examinees: result});
    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });

  router.get('/by-email', async (req, res) => {
    let email = req.query.email;
    if (!email) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You should include VALID email in query parameters.');
    }

    try {
      let user = await UserModel.findOne({email: email})
                  //.populate({ path: 'relationship.user'})
                  .exec();
      if (user) {
        delete user.passwordHash;
        if (user.role === 'examinee') {
          return res.status(200).json({examinee: user});
        }
        return util.sendHttpResponseMessage(res, MSG.clientError.conflict, null, 'Examiner is registered for email you provided.');
      }

      return res.status(200).json({examinee: user});
    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }
  });

  router.post('/create-examinee', async (req, res) => {

    try {
      let result = await UserModel.findOne({ email: req.body.email })
        .lean()
        .exec();

      if (result) {
        return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'User Exists');
      }

      let user = new UserModel(req.body);
      user.passwordHash = bcrypt.hashSync(req.body.password, 8);

      try {
        let userSaved = await user.save();
        let token = jwt.sign({ id: user._id }, config.SECRET, {
          expiresIn: config.JWT_TOKEN_EXPIRATION // expires in 24 hours
        });

        delete userSaved['passwordHash'];
        try {
          // add new examinee to referrals and relationship arrays
          await UserModel.findByIdAndUpdate(user.referrer, {$push: {'referrals': userSaved, 'relationship': {user: userSaved, type: 'examinee'}}}, {new: true});
          return res.status(200).json({user: userSaved });
        } catch (error) {
          return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
        }

      } catch (error) {
        return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
      }

    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error);
    }

  });

  return router;
};
