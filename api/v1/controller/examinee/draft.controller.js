module.exports = function(express) {
  let router = express.Router();
  let DraftModel = require('../../model/draft.model').model;
  let ExamOfferModel = require('../../model/exam-offer.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let mongoose = require('mongoose');

  router.post('/', async(req, res) => {
    if (!req.body) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'Test data should be provided.');
    }
    let validationFields = ['exam', 'test', 'examinee'];
    validationFields.forEach(field => {
      validateRequestBodyIDFields(field);
    });
    //__________________________________
    try {
      let draft = new DraftModel(req.body);
      let savedDraft = await draft.save();
      savedDraft = await DraftModel.populate(savedDraft, [{path: 'exam'}, {path: 'test', select: '-testGroups'}]);
      await ExamOfferModel.findByIdAndUpdate(draft.examOffer,
          {
            '$push': {'drafts':
                      {draft: savedDraft}
                    },
            status: 'accepted'
          },
          {new: true}
        );

      return res.status(200).json(savedDraft);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }

    //__________________________________
    function validateRequestBodyIDFields(field) {
      if (!req.body[field] || !mongoose.Types.ObjectId.isValid(req.body[field]) ) {
        return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, `You should provide VALID ${field}.`);
      }
    }
  });

  return router;
}
