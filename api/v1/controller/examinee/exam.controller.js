module.exports = function (express) {
  let router = express.Router();
  let ExamModel = require('../../model/exam.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let mongoose = require('mongoose');

  router.get('/:examId', async(req, res) => {
    let examId = req.params.examId;
    if (!examId || !mongoose.Types.ObjectId.isValid(examId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You must provide valid examId');
    }

    try {
      let exam = await ExamModel.findById(examId).exec();
      return res.status(200).json(exam);
    } catch (error) {
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }
  });

  return router;
}
