module.exports = function(express) {
  let router = express.Router();
  let ExamOfferModel = require('../../model/exam-offer.model').model;
  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let mongoose = require('mongoose');

  router.get('/examinee/:examineeId', async(req, res) => {
    let examineeId = req.params.examineeId;
    if (!examineeId || !mongoose.Types.ObjectId.isValid(examineeId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You must provide valid examineeId');
    }

    try {
      let offers = await ExamOfferModel.find({to: examineeId})
        .populate(
          [
            {path: 'from', select: '-passwordHash'},
            {path: 'to', select: '-passwordHash'},
            {path: 'exam', select: '-passwordHash'}
          ]
        )
        .exec();
      return res.status(200).json({offers: offers});
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }
  });


  return router;
}
