module.exports = function(express) {
  let router = express.Router();
  let DraftModel = require('../../model/draft.model').model;
  let TestModel = require('../../model/test-v2.model').model;

  let MSG = require('../../shared/messages/messages');
  let util = require('../../shared/util/util');
  let mongoose = require('mongoose');

  router.get('/full/load-test-by-draft-id/:draftId', async(req, res) => {
    let draftId = req.params.draftId;
    if (!draftId || !mongoose.Types.ObjectId.isValid(draftId)) {
      return util.sendHttpResponseMessage(res, MSG.clientError.badRequest, null, 'You must provide valid draftId');
    }

    try {
      let draft = await DraftModel.findById(draftId).exec();
      let test = await TestModel.findById(draft.test)
                        .select('-testGroups.groupItems.answers.correct')
                        .exec();

      res.status(200).json(test);
    } catch (error) {
      console.dir(error);
      return util.sendHttpResponseMessage(res, MSG.serverError.internalServerError, error, error.errmsg);
    }
  })

  return router;
}
