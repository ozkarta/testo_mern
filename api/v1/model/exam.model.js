let mongoose = require('mongoose');

let examSchema = new mongoose.Schema({
  title: {type: String, trim: true, required: true},
  test: {type: mongoose.Schema.Types.ObjectId, ref: 'Test-V2'},
  password: {type: String, required: true},
  owner: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
  results: [],
  offers: [{type: mongoose.Schema.Types.ObjectId, ref: 'ExamOffer'}]
}, {
  timestamps: true
});

let examModel = mongoose.model('Exam', examSchema);

exports.model = examModel;
exports.schema = examSchema;
