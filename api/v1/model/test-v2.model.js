let mongoose = require('mongoose');

let testV2Schema = new mongoose.Schema({
  title: {type: String, trim: true, required: true},
  friendlyId: {type: String, trim: true, unique: true, required: true},
  testProcessType: {type: String, enum: ['full', 'partial']},
  //partialStateQuestionQuantity: {type: Number, default: 10},  //  only if partial state is chosen
  defaultTimeForAnswering: {type: Number, default: 60}, // in seconds, in case of partial
  fullDuration: {type: Number, default: 60}, // in minutes,  in case of full process

  testGroups: [
    {
      groupTitle: {type: String, trim: true},
      groupIntroduction: {type: String, trim: true},
      groupQuestionGeneralCondition: {type: String, trim: true},
      groupItems: [{
        type: {type: String, enum: ['text', 'test'], default: 'test'},
        question: {type: String, default: '', trim: true},
        answers: [
          {
            answerText: {type: String, default: '', trim: true},
            correct: {type: Boolean, default: false}
          }
        ],
        point: {type: Number, default: 1},
        timeForAnswering: {type: Number, default: 60},
        correctionOffers: [
          {
            offerText: {
              type: String,
              default: '',
              trim: true
            }
          }
        ],
        disabled: {type: Boolean, default: false}
      }]
    }
  ],

  contributors: [{
    person: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    access: [
      'prohibited', // არაფრის უფლება არ აქვს ))
      'read',      // მხოლოდ დაათვალიეროს
      'moderate', // დასრულებული ტესტების გასწორების უფლება
      'full'      // ტესტების კონტენტის ჩასწორების უფლებაც აქვს
    ]
  }],
  owner: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true}
}, {
  timestamps: true
});

let testV2Model = mongoose.model('Test-V2', testV2Schema);

exports.model = testV2Model;
exports.schema = testV2Schema;
