let mongoose = require('mongoose');

let testSchema = new mongoose.Schema({
  title: {type: String, trim: true, required: true},
  friendlyId: {type: String, trim: true, unique: true, required: true},
  type: {type: String, enum: [
    'regular', // questions with estimated answers
    'mixed',  // questions with estimated answers + text answers
  ]},
  audiance: {type: String, emun: ['public', 'private']},
  status: {type: String, emun: ['published', 'pending']},
  state: {type: String, enum: ['full', 'partial']},  // testi bolomde unda gaiaros tu ara
  partialStateQuestionQuantity: {type: Number, default: 10},  //  only if partial state is chosen
  defaultTimeForAnswering: {type: Number, default: 60}, // in seconds
  contributors: [{
    person: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    access: [
      'prohibited', // არაფრის უფლება არ აქვს ))
      'read',      // მხოლოდ დაათვალიეროს
      'moderate', // დასრულებული ტესტების გასწორების უფლება
      'full'      // ტესტების კონტენტის ჩასწორების უფლებაც აქვს
    ]
  }],
  testItems: [{
    question: {type: String, default: '', trim: true},
    atachment: '',
    answers: [
      {
        answerText: {type: String, default: '', trim: true},
        correct: {type: Boolean, default: false}
      }
    ],
    multyAnswer: {type: Boolean, default: true},
    point: {type: Number, default: 1},
    defaultTimeForAnswering: {type: Number, default: 60},
    type: {type: String, default: '', trim: true},
    correctionOffers: [
      {
        offerText: {
          type: String,
          default: '',
          trim: true
        }
      }
    ],
    disabled: {type: Boolean, default: false}
  }],
  owner: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true}
}, {
  timestamps: true
});

let testModel = mongoose.model('Test', testSchema);

exports.model = testModel;
exports.schema = testSchema;
