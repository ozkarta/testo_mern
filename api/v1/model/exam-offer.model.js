let mongoose = require('mongoose');

let examOfferSchema = new mongoose.Schema({
  from: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  to: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  exam: {type: mongoose.Schema.Types.ObjectId, ref: 'Exam'},
  status: {type: String, enum: ['pending', 'accepeted', 'declined', 'done'], default: 'pending'},
  drafts: [
    {
      draft: {type: mongoose.Schema.Types.ObjectId, ref: 'Draft'},
      startDate: {type: Date, default: Date.now},
      endDate: {type: Date, default: null},
    }
  ]
}, {
  timestamps: true
});

let examOfferModel = mongoose.model('ExamOffer', examOfferSchema);

exports.model = examOfferModel;
exports.schema = examOfferSchema;
