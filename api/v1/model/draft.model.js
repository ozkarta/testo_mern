let mongoose = require('mongoose');

let draftSchema = new mongoose.Schema({
  exam: {type: mongoose.Schema.Types.ObjectId, ref: 'Exam'},
  test: {type: mongoose.Schema.Types.ObjectId, ref: 'Test-V2'},
  examOffer: {type: mongoose.Schema.Types.ObjectId, ref: 'ExamOffer'},
  examinee: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  examiner: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  testGroups: [
    {
      groupId: {type: String, required: true},
      groupItems: [
        {
          itemId: {type: String, required: true},
          type: {type: String, enum: ['text', 'test'], default: 'test'},
          answer: {
            textAnswer: {type: String, default: ''},
            testAnswers: [{type: String, default: ''}] // სავარაუდო პასუხის ID ჩაიწერება აქ
          },

          correct: {type: Boolean, default: false},
          maxPoint: {type: Number, default: 0},  // კითხვის სავარაუდო მაქსიმალური ქულა
          resultPoint: {type: Number, default: 0}, // კითხვაში მიღებული ქულა

          defaultTimeForAnswering: {type: Number, default: 60}, // მაქსიმალური დრო პასუხის გასაცემად
          resultTimeForAnswering: {type: Number, default: 60}, // დრო რომელიც დაჭირდა პასუხის გასაცემად
        }
      ]
    }
  ]
}, {
  timestamps: true
});

let draftModel = mongoose.model('Draft', draftSchema);

exports.model = draftModel;
exports.schema = draftSchema;
