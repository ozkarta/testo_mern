let mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
  firstName: {type: String, trim: true},
  lastName: {type: String, trim: true},
  email: {type: String, trim: true},
  passwordHash: {type: String},
  role: {type: String, enum: ['examiner', 'examinee']},
  referrals: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  referrer: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  relationship: [
    {
      user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
      type: {type: String, enum: ['examinee', 'examiner']}
    }
  ]
}, {
  timestamps: true
});

let userModel = mongoose.model('User', userSchema);

exports.model = userModel;
exports.schema = userSchema;
