import React, {Component} from 'react';
export default class ErrorAlertComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    if (this.props && this.props.visible) {
      return(
        <div className="alert alert-danger">
          <strong>Danger!</strong> {this.props.msg}
        </div>
      );
    }

    return null;
  }
}
