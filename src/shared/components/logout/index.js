import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as UserActions from '../../actions/user.actions';

class LogOutComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }
  render() {
    return(
      <button type="button" name="button" onClick={() => {this.props.dispatch(UserActions.logOutUser())}}> {'Log Out'}</button>
    );
  }
}

export default connect(
    (state) => {
        return {
            user: state.user
        };
    }
)(LogOutComponent)
