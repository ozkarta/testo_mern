import React, {Component, Fragment} from 'react';

export default class TimerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };

    this.started= new Date();
    this.calculateElapsedTime();
  }

  calculateElapsedTime() {

    this.id = setInterval(() => {
      let now = new Date();
      let reduced = this.props.endDate - now;
      let stateCP = Object.assign({}, this.state);

      const elapsed = {
        hour:  this.checkTime(Math.floor(( reduced ) / 1000 / 60 / 60)),
        minutes:  this.checkTime(Math.floor(( reduced ) / 1000 / 60)),
        seconds:  this.checkTime(Math.floor(( reduced ) / 1000 % 60))
      };

      if (reduced <= 0) {
        elapsed.hour = '00';
        elapsed.minutes = '00';
        elapsed.seconds = '00';
        this.props.timeIsUpHandler();
        clearInterval(this.id);
      }

      stateCP.elapsed = elapsed;
      this.setState(stateCP);

    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.id);
  }

  checkTime(tm) {
    if (tm <10) {
      return `0${tm}`;
    }
    return `${tm}`;
  }


  render() {

    return(
      <Fragment>
        <h3>Started: {this.started.toLocaleTimeString()}</h3>
        <h3>end    : {this.props.endDate && this.props.endDate.toLocaleTimeString()}</h3>
        <h3>Elapsed: {this.state.elapsed && `${this.state.elapsed.hour}: ${this.state.elapsed.minutes}: ${this.state.elapsed.seconds}`}</h3>
      </Fragment>
    );
  }
}
