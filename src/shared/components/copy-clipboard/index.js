import React, {Component, Fragment} from 'react';

export default class CopyClipboardComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  render() {
    return(
      <Fragment>
        <input type="text" disabled={true} value={this.props.clipBoardText} ref={'clipboardRef' + (this.props.index || 0)}/>
        <button type="button" onClick={(event) => {
              event.preventDefault();
              const clipboardRef = this.refs['clipboardRef'+(this.props.index || 0)];
              if (!clipboardRef) {
                return;
              }
              clipboardRef.disabled = false;
              clipboardRef.select();
              document.execCommand('copy');
              clipboardRef.disabled = true;
            }}>
          Copy
        </button>
      </Fragment>
    );
  }
}
