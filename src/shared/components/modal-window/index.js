import React, {Component, Fragment} from 'react';

export default class ModalWindowComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    if (!this.props.show) {
      return null;
    }
    return(
      <Fragment>
        <div className="modal-backdrop show"></div>
        <div className="modal" id="myModal" style={{display: 'block'}}>
          <div className="modal-dialog modal-lg">
            <div className="modal-content">

              <div className="modal-header">
                <h4 className="modal-title">{this.props.modalTitle}</h4>
                <button type="button" className="close" data-dismiss="modal" onClick={this.props.modalExitClickHandler}>&times;</button>
              </div>

              <div className="modal-body">
                {this.props.modalBody}
              </div>

              <div className="modal-footer">
                {
                  this.props.modalSubmitClickHandler && typeof this.props.modalSubmitClickHandler ==='function' &&
                  (<button type="button" className="btn btn-danger" data-dismiss="modal" onClick={this.props.modalSubmitClickHandler}>{this.props.submitButtonText || 'Save'}</button>)
                }
                {
                  this.props.modalDiscardClickHandler && typeof this.props.modalDiscardClickHandler === 'function' &&
                  (<button type="button" className="btn btn-danger" data-dismiss="modal" onClick={this.props.modalDiscardClickHandler}>{this.props.discardButtonText || 'Discard'}</button>)
                }
              </div>

            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
