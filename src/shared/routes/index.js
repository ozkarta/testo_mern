import React from 'react';
import { Route, Switch, Redirect } from "react-router-dom";

// Visitor
import VisitorLandingComponent from '../../visitor/components/landing';
import LoginComponent from '../../visitor/components/login';
import RegisterComponent from '../../visitor/components/register';
// import VisitorLogIn from '../user/visitor/components/login/login';
// import VisitorRegister from '../user/visitor/components/register/register';

// Examiner
import ExaminerTestsComponent from '../../examiner/components/tests/v2';
import TestsFormComponent from '../../examiner/components/tests/v2/tests.form.js';
import ExaminerTeamsComponent from '../../examiner/components/teams';
import ExaminerExamsComponent from '../../examiner/components/exams';
import ExaminerStatisticsComponent from '../../examiner/components/statistics';
import TestContentEditorComponent from '../../examiner/components/test-content-editor/v2';
import ExaminerExamineesComponent from '../../examiner/components/examinees';

// Examinee
import ExamineeIndexComponent from '../../examinee/components/index';
import ExamineeExamListComponent from '../../examinee/components/exam-list';
import ExamineeLoginComponent from '../../examinee/components/login';
import ExamineeAccountComponent from '../../examinee/components/account';
import ExamineeIndexHistoryComponent from '../../examinee/components/exam-history';
import ExamineeHelpAndSupportComponent from '../../examinee/components/help-and-support';
import ExamineeNotificationsComponent from '../../examinee/components/notifications';
import ExamineeExamComponent from '../../examinee/components/exam';
// Shared
import PageNotFound from '../components/404-not-found';

class Routes extends React.Component {
    routeConfig = [
        // Visitor Routes
        {
            route: { exact: true, path: '/', component: VisitorLandingComponent },
            routeUserRole: 'visitor',
            authRequired: false,
            accessFromAuthenticated: false
        },
        {
            route: { exact: false, path: '/login/:redirect?', component: LoginComponent },
            routeUserRole: 'visitor',
            authRequired: false,
            accessFromAuthenticated: false
        },
        {
            route: { exact: true, path: '/register/:redirect?', component: RegisterComponent },
            routeUserRole: 'visitor',
            authRequired: false,
            accessFromAuthenticated: false
        },



        // Examiner routes
        {
            route: { exact: true, path: '/examiner', component: ExaminerTestsComponent },
            routeUserRole: 'examiner',
            authRequired: true
        },
        {
            route: { exact: true, path: '/examiner/tests', component: ExaminerTestsComponent },
            routeUserRole: 'examiner',
            authRequired: true
        },
        {
            route: { exact: true, path: '/examiner/tests/new', component: TestsFormComponent },
            routeUserRole: 'examiner',
            authRequired: true
        },
        {
            route: { exact: true, path: '/examiner/tests/:testId/content', component: TestContentEditorComponent },
            routeUserRole: 'examiner',
            authRequired: true
        },
        {
            route: { exact: true, path: '/examiner/teams', component: ExaminerTeamsComponent },
            routeUserRole: 'examiner',
            authRequired: true
        },
        {
            route: { exact: true, path: '/examiner/exams', component: ExaminerExamsComponent },
            routeUserRole: 'examiner',
            authRequired: true
        },
        {
            route: { exact: true, path: '/examiner/statistics', component: ExaminerStatisticsComponent },
            routeUserRole: 'examiner',
            authRequired: true
        },
        {
            route: { exact: true, path: '/examiner/examinees/:action?/:email?', component: ExaminerExamineesComponent },
            routeUserRole: 'examiner',
            authRequired: true
        },

        // Examinee routes
        // {
        //     route: { exact: true, path: '/examinee', component: ExamineeIndexComponent },
        //     routeUserRole: 'examinee',
        //     authRequired: true
        // },
        // {
        //     route: { exact: true, path: '/examinee/index', component: ExamineeIndexComponent },
        //     routeUserRole: 'examinee',
        //     authRequired: true
        // },

        {
            route: { exact: true, path: '/examinee/login/:redirect?', component: ExamineeLoginComponent },
            routeUserRole: 'visitor',
            authRequired: false
        },
        {
            route: { exact: true, path: '/examinee/', component: ExamineeIndexComponent },
            routeUserRole: 'examinee',
            authRequired: true
        },
        {
            route: { exact: true, path: '/examinee/home', component: ExamineeIndexComponent },
            routeUserRole: 'examinee',
            authRequired: true,
        },
        {
            route: { exact: true, path: '/examinee/upcomming-exams', component: ExamineeExamListComponent },
            routeUserRole: 'examinee',
            authRequired: true,
            redirect: '/examinee/login'
        },

        {
            route: { exact: true, path: '/examinee/account', component: ExamineeAccountComponent },
            routeUserRole: 'examinee',
            authRequired: true,
            redirect: '/examinee/login'
        },
        {
            route: { exact: true, path: '/examinee/exams-history', component: ExamineeIndexHistoryComponent },
            routeUserRole: 'examinee',
            authRequired: true,
            redirect: '/examinee/login'
        },
        {
            route: { exact: true, path: '/examinee/help-support', component: ExamineeHelpAndSupportComponent },
            routeUserRole: 'examinee',
            authRequired: true,
            redirect: '/examinee/login'
        },
        {
            route: { exact: true, path: '/examinee/notifications', component: ExamineeNotificationsComponent },
            routeUserRole: 'examinee',
            authRequired: true,
            redirect: '/examinee/login'
        },
        {
            route: { exact: true, path: '/examinee/exam', component: ExamineeExamComponent },
            routeUserRole: 'examinee',
            authRequired: true,
            redirect: '/examinee/login'
        },


        // {
        //     route: { path: '/login', component: VisitorLogIn },
        //     routeUserRole: 'visitor',
        //     authRequired: false,
        //     accessFromAuthenticated: false
        // },
        // {
        //     route: { path: '/register', component: VisitorRegister },
        //     routeUserRole: 'visitor',
        //     authRequired: false,
        //     accessFromAuthenticated: false
        // },

        // // Buyer Routes

        // {
        //     route: { path: '/buyer/account', component: BuyerAccount },
        //     routeUserRole: 'buyer',
        //     authRequired: true
        // },
        // {
        //     route: { path: '/buyer/profile', component: BuyerProfile },
        //     routeUserRole: 'buyer',
        //     authRequired: true
        // },
        //
        // // Seller Routes
        // {
        //     route: { exact: true, path: '/seller', component: SellerHome },
        //     routeUserRole: 'seller',
        //     authRequired: true
        // },
        // {
        //     route: { path: '/seller/account', component: SellerAccount },
        //     routeUserRole: 'seller',
        //     authRequired: true
        // },
        // {
        //     route: { path: '/seller/business-profile', component: SellerBusinessProfile },
        //     routeUserRole: 'seller',
        //     authRequired: true
        // },
        // {
        //     route: { path: '/seller/store', component: SellerStore },
        //     routeUserRole: 'seller',
        //     authRequired: true
        // },
        // {
        //     route: { path: '/seller/products', component: SellerProduct },
        //     routeUserRole: 'seller',
        //     authRequired: true
        // },
    ];
    constructor(props) {
        super(props);
        this.state = props;
    }

    render() {
        return (
            <React.Fragment>
                <GenerateSwitch routeConfig={this.routeConfig} parentState={this.props} />
            </React.Fragment>
        );
    }
}

class GenerateSwitch extends React.Component {
    constructor(props) {
        super(props);
        this.state = props;
        this.renderHandler = this.renderHandler.bind(this);
    }

    renderHandler(props, routeItem, Component) {
        // Routes with authenticationonly
        // Redirect in other case
        if (routeItem.authRequired) {
            if (!this.props.parentState.user.isAuthenticated) {
                if (routeItem.redirect) {
                  return (
                      <Redirect
                          to={{
                              pathname: `${routeItem.redirect}${props.location.pathname? `/${props.location.pathname.substring(1, props.location.pathname.length).replace(/\//g, '-')}` : ''}`,
                              state: { from: props.location },
                          }}
                      />
                  );
                }
                return (
                    <Redirect
                        to={{
                            pathname: `/login${props.location.pathname? `/${props.location.pathname.substring(1, props.location.pathname.length).replace(/\//g, '-')}` : ''}`,
                            state: { from: props.location },
                        }}
                    />
                );
            }

            if (this.props.parentState.user.role !== routeItem.routeUserRole) {
                if (routeItem.redirect) {
                  return (
                      <Redirect
                          to={{
                              pathname: `${routeItem.redirect}${props.location.pathname? `/${props.location.pathname.substring(1, props.location.pathname.length).replace(/\//g, '-')}` : ''}`,
                              state: { from: props.location },
                          }}
                      />
                  );
                }
                return (
                    <Redirect
                        to={{
                            pathname: `/login${props.location.pathname? `/${props.location.pathname.substring(1, props.location.pathname.length).replace(/\//g, '-')}` : ''}`,
                            state: { from: props.location },
                        }}
                    />
                );
            }

        } else {
            // Route which require unauthenticated user access
            if (!routeItem.accessFromAuthenticated) {
                if (this.props.parentState.user.isAuthenticated) {
                    return (
                        <Redirect
                            to={{
                                pathname: `/${this.props.parentState.user.role}`,
                                state: { from: props.location },
                            }}
                        />
                    );
                }
            }
        }


        // Routes which don't require authentication
        // and can be accessed by authenticated users too
        return (<Component {...props} />);
    }

    render() {
        return (
            <React.Fragment>
                <Switch>
                    {this.state.routeConfig.map((routeItem, i) => {
                        let route = Object.assign({}, routeItem.route);
                        delete route.component;

                        return (<Route key={`${routeItem.routeUserRole}_${i}_${routeItem.route.path}`}
                            {...route}
                            render={(props) => this.renderHandler(props, routeItem, routeItem.route.component)}
                        />);
                    })}

                    <Route component={PageNotFound} />
                </Switch>
            </React.Fragment>
        );
    }
}

export default Routes;
