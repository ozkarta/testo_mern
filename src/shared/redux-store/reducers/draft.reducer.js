const draft = localStorage.getItem('draft')? JSON.parse(localStorage.getItem('draft')) : null;

const DraftReducer = function(state = draft, action) {
    if (action.type === 'EXAM_STARTED') {
        localStorage.setItem('draft', JSON.stringify(action.draft));
        return Object.assign({}, state, {...action.draft});
    }

    if (action.type === 'DRAFT_CHANGED') {
        localStorage.removeItem('draft');
        localStorage.setItem('draft', JSON.stringify(action.draft));
        return Object.assign({}, state, {...action.draft});
    }
    return state;
}

export default DraftReducer;
