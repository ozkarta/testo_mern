import {createStore, combineReducers} from 'redux';
import UserReducer from './reducers/user.reducer';
import BusyIndicatorReduced from './reducers/busy-indicator.reducer';
import DraftReducer from './reducers/draft.reducer';
const combinedReducer = combineReducers({
    user: UserReducer,
    busyIndicator: BusyIndicatorReduced,
    draftObject: DraftReducer
});

const store = createStore(combinedReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

store.subscribe(() => {

});

export default store;
