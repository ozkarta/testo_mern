import React, {Component} from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {connect} from 'react-redux';
import axios from 'axios';

import Routes from './shared/routes';
import './App.css';

axios.defaults.baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:4000' : '';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }
  render() {
    return (
        <React.Fragment>
          <Router>
            <React.Fragment>


              <Routes {...this.props}/>
            </React.Fragment>
          </Router>
        </React.Fragment>
    );
  }
}

export default connect(
    (state) => {
        return {
            user: state.user
        };
    }
)(App)
