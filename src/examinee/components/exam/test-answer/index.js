import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import './index.css';
class TestAnswerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
    this.radioChangeHandler = this.radioChangeHandler.bind(this);
    this.isRadioChecked = this.isRadioChecked.bind(this);
  }

  radioChangeHandler(event, groupItemId) {
    let stateCP = Object.assign({}, this.state);
    stateCP.draft.testGroups.forEach(group => {
      if (group['groupId'] === stateCP.testGroupId) {
        group.groupItems.forEach(item => {
          if (item['itemId'] === groupItemId) {
            item.answer.testAnswer = event.target.value;
            this.state.dispatch({type: 'DRAFT_CHANGED', draft: stateCP.draft});
          }
        })
      }
    });
  }

  isRadioChecked(groupItemId, answerId) {
    let stateCP = Object.assign({}, this.state);
    let value = false;
    stateCP.draft.testGroups.forEach(group => {
      if (group['groupId'] === stateCP.testGroupId) {
        group.groupItems.forEach(item => {
          if (item['itemId'] === groupItemId) {
            if (item.answer.testAnswer === answerId) {
              value = true;
            }
          }
        })
      }
    })

    return value;
  }

  render() {

    return(
      <Fragment>
        {
          this.state.groupItem && this.state.groupItem.answers && this.state.groupItem.answers.map((answer, index) => {
            return(
              <div className="radio" key={index}>
                <label>
                  <input type="radio"
                    name={this.state.groupItem['_id']} value={answer['_id']}
                    onChange={(event) => {
                      this.radioChangeHandler(event, this.state.groupItem['_id'])
                    }}
                    checked={this.isRadioChecked(this.state.groupItem['_id'], answer['_id'])}/>
                    {answer.answerText}
                </label>
              </div>

            );
          })
        }
      </Fragment>
    );
  }
}

export default connect(
    (state) => {
        return {
          draft: state.draftObject
        };
    }
)(TestAnswerComponent)
