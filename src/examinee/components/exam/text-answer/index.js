import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import './index.css'
class TextAnswerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
    this.defaultChangeHandler = this.defaultChangeHandler.bind(this);
  }

  defaultChangeHandler(event) {
    let stateCP = Object.assign({}, this.state);
    stateCP.draft.testGroups.forEach(group => {
      if (group['groupId'] === stateCP.testGroupId) {
        group.groupItems.forEach(item => {
          if (item['itemId'] === stateCP.groupItem['_id']) {
            item.answer.textAnswer = event.target.value;
            this.state.dispatch({type: 'DRAFT_CHANGED', draft: stateCP.draft});
          }
        })
      }
    });
  }

  getValueFromDraft() {
    let stateCP = Object.assign({}, this.state);
    let value = false;
    stateCP.draft.testGroups.forEach(group => {
      if (group['groupId'] === stateCP.testGroupId) {
        group.groupItems.forEach(item => {
          if (item['itemId'] === stateCP.groupItem['_id']) {
            value = item.answer.textAnswer;
          }
        })
      }
    })

    return value || '';
  }

  render() {

    return(
      <Fragment>
        {
          <div className="form-group">
            <textarea className="form-control" id="exampleTextarea" rows="3"
              value={this.getValueFromDraft()}
              onChange={this.defaultChangeHandler}></textarea>
          </div>
        }
      </Fragment>
    );
  }
}

export default connect(
    (state) => {
        return {
          draft: state.draftObject
        };
    }
)(TextAnswerComponent)
