import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import * as ExamineeExamProcessActions from '../../actions/exam-process.actions';
import TestAnswerComponent from '../test-answer';
import TextAnswerComponent from '../text-answer';
import TimerComponent from '../../../../shared/components/timer';
import './index.css';

class ExamineeExamFullProcessComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
    };
    this.timeIsUpHandler = this.timeIsUpHandler.bind(this);
    this.loadExamByDraftId = this.loadExamByDraftId.bind(this);
    this.remakeDraftObject = this.remakeDraftObject.bind(this);
    this.examSubmitClickHandler = this.examSubmitClickHandler.bind(this);
  }

  componentDidMount() {
    if (!this.state.draft) {
      this.state.history.push('/examinee/upcomming-exams')
    }
    this.loadExamByDraftId(this.state.draft['_id']);
  }

  startTimer(stateCP) {
    let duration = parseInt(this.state.draft.test.fullDuration, 10); //  in minutes
    let endDate = new Date(this.state.draft.createdAt);
    endDate.setTime(endDate.getTime() + ( duration * 60 * 1000 ));  // milliseconds
    if ( (endDate - new Date()) <=0 ) {
      console.log('expired....');
      this.closeDraftAndRemoveFromMemory(stateCP);
    } else {
      stateCP.endDate = endDate;
      this.setState(stateCP);
    }

  }

  closeDraftAndRemoveFromMemory(stateCP) {
    console.log('Closing draft ... DB');
    console.log('Dispatch CLOSE_DRAFT');
    console.log('Disable all inputs and show warning...');
    this.setState(stateCP);
  }

  timeIsUpHandler() {
    console.log('Time Is Up');
  }

  loadExamByDraftId(draftId) {
    let stateCP = Object.assign({}, this.state);

    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExamineeExamProcessActions.loadFullExamByDraftId(draftId)
      .then(test => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.test = test;
        //this.setState(stateCP);

        this.remakeDraftObject(test);
        this.startTimer(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  remakeDraftObject(test) {
    if (!test) {
      return;
    }
    let draftCP = Object.assign({}, this.state.draft);
    if (!draftCP.testGroups) {
      draftCP.testGroups = [];
    }

    test.testGroups.forEach(testGroup => {
      let exists = false;
      draftCP.testGroups.forEach(gr => {
        if (gr['groupId'] === testGroup['_id']) {
          exists = true;
        }
      });
      if (exists) {
        return;
      }
      let group = {};
      group.groupId = testGroup['_id'];
      group.groupItems = [];
      testGroup.groupItems && testGroup.groupItems.forEach(groupItem => {
        let item = {};
        item.itemId = groupItem['_id'];
        item.type = groupItem.type;
        item.answer = {
          textAnswer: '',
          testAnswer: ''
        };
        item.correct = false;
        item.maxPoint = groupItem.point;
        item.resultPoint = 0;
        item.defaultTimeForAnswering = groupItem.defaultTimeForAnswering;
        item.resultTimeForAnswering = groupItem.defaultTimeForAnswering;
        group.groupItems.push(item);
      })
      draftCP.testGroups.push(group);
    });
  }

  examSubmitClickHandler() {
    console.dir(this.state.draft);
  }

  render() {
    let prevQTY = 0;
    if (!(this.state && this.state.test )) {
      return null;
    }
    return(
      <Fragment>
        {
          this.state.endDate &&
          <TimerComponent startDate={this.state.draft.createdAt} endDate={this.state.endDate} timeIsUpHandler={this.timeIsUpHandler}/>
        }

        {
          this.state.test.testGroups.map((testGroup, index) => {
            prevQTY += testGroup.groupItems.length;
            return(
              <Fragment key={index}>
                <hr/>
                <TestGroupComponent testGroup={testGroup} startIndex={prevQTY}/>
              </Fragment>
            )
          })
        }

        <button type="button" onClick={this.examSubmitClickHandler} >Submit Exam</button>
      </Fragment>
    );
  }
}

class TestGroupComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    if (!this.props || !this.props.testGroup) {
      return null;
    }
    return(
      <Fragment>
        <h2>{this.props.testGroup.groupTitle}</h2>
        <p dangerouslySetInnerHTML={{__html: this.props.testGroup.groupIntroduction}} />

        {
          this.props.testGroup.groupQuestionGeneralCondition &&
          <p dangerouslySetInnerHTML={{__html: this.props.testGroup.groupQuestionGeneralCondition}} />
        }

        {
          this.props.testGroup.groupItems && this.props.testGroup.groupItems.map((groupItem, index) => {

            return(
              <Fragment key={index}>
                <TestItemQuestionComponent groupItem={groupItem}  groupItemIndex={index} startIndex={this.props.startIndex}/>
                {
                  groupItem.type === 'test' &&
                  <TestAnswerComponent groupItem={groupItem} testGroupId={this.props.testGroup['_id']}/>
                }

                {
                  groupItem.type === 'text' &&
                  <TextAnswerComponent groupItem={groupItem} testGroupId={this.props.testGroup['_id']}/>
                }
              </Fragment>
            );

          })
        }
      </Fragment>
    );
  }
}

class TestItemQuestionComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  render() {
    return(
      <Fragment>
        <div className="row">
          <h2>{this.props.startIndex + this.props.groupItemIndex}. </h2>
          <h3>{this.props.groupItem.question}</h3>
        </div>
      </Fragment>
    );
  }
}


export default connect(
    (state) => {
        return {
          user: state.user,
          draft: state.draftObject
        };
    }
)(ExamineeExamFullProcessComponent)
