import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../template';
import ExamineeExamFullProcessComponent from './full-exam-process';
import ExamineeExamPartialProcessComponent from './partial-exam-process';
import './index.css';

class ExamineeExamComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  componentDidMount() {
    if (!this.state.draft) {
      this.state.history.push('/examinee/upcomming-exams')
    }
  }

  render() {
    return(
      <ExaminerTemplateComponent child={(
          <Fragment>
            {
              this.state.draft && this.state.draft.test && this.state.draft.test.testProcessType==='full' &&
              <ExamineeExamFullProcessComponent />
            }

            {
              this.state.draft && this.state.draft.test && this.state.draft.test.testProcessType==='partial' &&
              <ExamineeExamPartialProcessComponent />
            }
          </Fragment>
        )}/>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user,
          draft: state.draftObject
        };
    }
)(ExamineeExamComponent)
