import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import './index.css';

class ExamineeExamPartialProcessComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  componentDidMount() {
    console.dir(this.state);
  }

  render() {
    return(
      <Fragment>
        <h3>Partial Component</h3>
      </Fragment>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user,
          draft: state.draftObject
        };
    }
)(ExamineeExamPartialProcessComponent)
