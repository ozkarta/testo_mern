import axios from 'axios';

const apiBaseUrl = '/api/v1';

export async function createDraft(draft) {
    try {
        const response =  await axios.post(`${apiBaseUrl}/examinee/draft/`, draft);
        return (response && response.data) ? response.data : null;
    } catch (ex) {
        throw ex.response.data;
    }
}
