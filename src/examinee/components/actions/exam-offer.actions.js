import axios from 'axios';

const apiBaseUrl = '/api/v1';

export async function getExamOfferForExaminee(examineeId) {
  try {
      const response =  await axios.get(`${apiBaseUrl}/examinee/exam-offer/examinee/${examineeId}`);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}
