
import axios from 'axios';

const apiBaseUrl = '/api/v1';

export async function loadFullExamByDraftId(draftId) {
  try {
      const response =  await axios.get(`${apiBaseUrl}/examinee/exam-process/full/load-test-by-draft-id/${draftId}`);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}
