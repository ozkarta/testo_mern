import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import LogOutComponent from '../../../shared/components/logout';

class NavbarComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <Link className="nav-link" to={'/'}>TestO</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">

            <li className="nav-item active">
              <Link className="nav-link active" to="/">
                Home
              </Link>
            </li>

            {
              this.state.draft &&
              <li className="nav-item active">
                <Link className="nav-link active" to="/examinee/exam">
                  Back To Exam
                </Link>
              </li>
            }

            <li className="nav-item active">
              <Link className="nav-link active" to="/examinee/upcomming-exams">
                Upcommint Exams
              </Link>
            </li>

            <li className="nav-item active">
              <Link className="nav-link active" to="/examinee/exams-history">
                Exams History
              </Link>
            </li>

            <li className="nav-item active">
              <Link className="nav-link active" to="/examinee/notifications">
                Notificactions
              </Link>
            </li>

            <li className="nav-item active">
              <Link className="nav-link active" to="/examinee/account">
                Account
              </Link>
            </li>

            <li className="nav-item active">
              <Link className="nav-link active" to="/examinee/help-support">
                Help And Support
              </Link>
            </li>

          </ul>
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <LogOutComponent/>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default connect(
    (state) => {
        return {
            user: state.user,
            draft: state.draftObject
        };
    }
)(NavbarComponent)
