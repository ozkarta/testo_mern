import React, {Component} from 'react';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../template';

import './index.css';

class ExamineeNotificationsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  render() {
    return(
      <ExaminerTemplateComponent child={(<h1>Notifications</h1>)}/>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExamineeNotificationsComponent)
