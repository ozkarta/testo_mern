import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../template';
//import * as ExamineeExamActions from '../actions/exam.actions';
import * as ExamineeExamOfferActions from '../actions/exam-offer.actions';
import * as ExamineeDraftActions from '../actions/draft.actions';
import ModalWindowComponent from '../../../shared/components/modal-window';
import './index.css';

class ExamineeExamListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      showStartExamSubmitModal: false
    };
  }

  render() {
    return(
      <ExaminerTemplateComponent child={(
        <Fragment>
          <ExamComponent {...this.state}/>
        </Fragment>
      )}/>
    );
  }
}

class ExamComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };

    //this.getExamById = this.getExamById.bind(this);
    this.getExamineeExamOffers = this.getExamineeExamOffers.bind(this);
    this.startExamClickHandler = this.startExamClickHandler.bind(this);
    this.startExamModalWindowHandlers = this.startExamModalWindowHandlers.bind(this);
    this.createDraft = this.createDraft.bind(this);
  }

  componentDidMount() {
    let userId = this.props.user.user['_id'];
    this.getExamineeExamOffers(userId);
  }

  getExamineeExamOffers(examineeId) {
    let stateCP = Object.assign({}, this.state);

    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExamineeExamOfferActions.getExamOfferForExaminee(examineeId)
      .then(result => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.examOffers = result? result.offers : [];
        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  createDraft(draft) {
    let stateCP = Object.assign({}, this.state);

    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExamineeDraftActions.createDraft(draft)
      .then(result => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        this.state.dispatch({type: 'EXAM_STARTED', draft: result});
        this.state.history.push('/examinee/exam');
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  startExamClickHandler(examOffer) {
    let stateCP = Object.assign({}, this.state);
    stateCP.showStartExamSubmitModal = true;
    stateCP.examOfferToStart = examOffer;
    this.setState(stateCP);
  }

  startExamModalWindowHandlers() {
    let handlers = {};
    handlers.modalExitClickHandler = () => {
      let stateCP = Object.assign({}, this.state);
      stateCP.showStartExamSubmitModal = false;
      stateCP.examOfferToStart = null;
      this.setState(stateCP);
    };
    handlers.modalDiscardClickHandler = () => {
      let stateCP = Object.assign({}, this.state);
      stateCP.showStartExamSubmitModal = false;
      stateCP.examOfferToStart = null;
      this.setState(stateCP);
    };
    handlers.modalSubmitClickHandler = () => {
      let stateCP = Object.assign({}, this.state);
      stateCP.showStartExamSubmitModal = false;
      stateCP.examOfferToStart = null;

      let draftObject = Object.assign({});
      draftObject.test = this.state.examOfferToStart.exam['test'];
      draftObject.exam = this.state.examOfferToStart.exam['_id'];
      draftObject.examOffer = this.state.examOfferToStart['_id'];
      draftObject.examinee = this.props.user.user['_id'];
      draftObject.examiner = this.state.examOfferToStart['from']['_id'];
      draftObject.testGroups = [];
      this.setState(stateCP);

      this.createDraft(draftObject);
    }
    return handlers;
  }

  render() {
    return(
      <Fragment>
        <h1>Examinee Exam</h1>
        <ModalWindowComponent
            {...this.startExamModalWindowHandlers()}
            show={this.state.showStartExamSubmitModal}
            submitButtonText={'Start Exam'}
            discardButtonText={'Cancel'}
            modalBody={
              (
                <Fragment>
                  <h3>Are you sure you want to start Exam?</h3>
                  <p>!!! Notice !!! you will not be able to pause exam......</p>
                </Fragment>
              )
            }
            />
        <ExamListComponent {...this.state} startExamClickHandler={this.startExamClickHandler}/>
      </Fragment>
    );
  }
}

class ExamListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return(
      <Fragment>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>Sent On</th>
              <th>Title</th>
              <th>From</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.examOffers && this.props.examOffers.map((examOffer, index) => {
                return(
                  <tr key={index}>
                    <td>{examOffer.createdAt}</td>
                    <td>{examOffer.exam.title}</td>
                    <td>{`${examOffer.from['firstName']} ${examOffer.from['lastName']}`}</td>
                    <td>
                      <button type="button" onClick={() => this.props.startExamClickHandler(examOffer)}>Start</button>
                      <button type="button">Decline</button>
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      </Fragment>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExamineeExamListComponent)
