import React, {Component, Fragment} from 'react';
import NavbarComponent from '../navbar';
//import NavbarComponent from '../../../visitor/components/navbar';
import './index.css';

export default class ExamineeTemplateComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return(
      <Fragment>
        <div className="container-fluid">
          <div className="row">
            <NavbarComponent/>
            <main className="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
              <br/><br/>
              {this.props.child}
            </main>

          </div>
        </div>
      </Fragment>
    );
  }
}
