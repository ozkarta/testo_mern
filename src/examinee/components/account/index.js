import React, {Component} from 'react';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../template';

import './index.css';

class ExamineeAccountComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  render() {
    return(
      <ExaminerTemplateComponent child={(<h1>Account</h1>)}/>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExamineeAccountComponent)
