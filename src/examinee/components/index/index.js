import React, {Component} from 'react';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../template';

import './index.css';

class ExamineeIndexComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  render() {
    return(
      <ExaminerTemplateComponent child={(<h1>Examinee Langing(Index)</h1>)}/>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExamineeIndexComponent)
