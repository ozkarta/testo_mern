import React, {Component} from 'react';
import {connect} from 'react-redux';
// import ExaminerTemplateComponent from '../template';
import ErrorAlertComponent from '../../../shared/components/error-alert';
import './index.css';

class ExamineeLoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      user: {
        email: 'ozbegi2@gmail.com',
        password: '12qwert12'
      }
    };

    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.formSubmitEventHandler = this.formSubmitEventHandler.bind(this);
  }


  inputChangeHandler(value, field) {
    let stateCP = Object.assign({}, this.state);
    stateCP.user[field] =  value;
    this.setState(stateCP);
  }

  formSubmitEventHandler(event) {
    event.preventDefault();
    console.dir(this.state.user);
  }

  render() {
    return(

      <React.Fragment>
        <br/>
        <br/>
        <br/>

      <ErrorAlertComponent {...this.state.error}/>

        <form onSubmit={this.formSubmitEventHandler}>
          <div className="form-group">
            <label htmlFor="email">Email address:</label>
            <input type="email" className="form-control" id="email"
              value={this.state.user.email}
              onChange={(event) => {this.inputChangeHandler(event.target.value, 'email')}}/>
          </div>
          <div className="form-group">
            <label htmlFor="pwd">Password:</label>
            <input type="password" className="form-control" id="pwd"
              value={this.state.user.password}
              onChange={(event) => {this.inputChangeHandler(event.target.value, 'password')}}/>
          </div>
          <div className="form-group form-check">
            <label className="form-check-label">
              <input className="form-check-input" type="checkbox"/> Remember me
            </label>
          </div>
          <button type="submit" className="btn btn-primary">Log In</button>
        </form>
      </React.Fragment>

    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExamineeLoginComponent)
