import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../template';
import ExaminerExamineeFormComponent from './examinees.form';
import ModalWindowComponent from '../../../shared/components/modal-window';
import * as ExaminerExamineeActions from '../actions/examinee.actions';
import './index.css';
class ExaminerExamineesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      showCreateNewExamineeWindowModal: false,
      isUpdateForm: false
    };
    this.loadExamineesInRelationship = this.loadExamineesInRelationship.bind(this);
    this.modalWindowAttributes = this.modalWindowAttributes.bind(this);
    this.createNewExamineeData = this.createNewExamineeData.bind(this);
    this.updateNewExamineeData = this.updateNewExamineeData.bind(this);
    this.createNewExamineeRequest = this.createNewExamineeRequest.bind(this);
  }

  componentDidMount() {
    let action = this.state.match.params.action;

    let stateCP = null;
    if (action && action === 'new-examinee') {
      let data = {};
      if (this.state.match.params.email) {
        data.email = this.state.match.params.email
      }
      stateCP = this.createNewExamineeData(data, true);
    }
    this.loadExamineesInRelationship(stateCP);
  }

  loadExamineesInRelationship(_stateCP) {
    let userId = this.state.user.user['_id'];
    let stateCP = _stateCP || Object.assign({}, this.state);
    console.dir(stateCP);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerExamineeActions.getExamineeRelationships(userId)
      .then(result => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.myExaminees = result.examinees;
        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  modalWindowAttributes(isUpdateForm) {
    let handlers = {};
    handlers.modalExitClickHandler = (event) => {
      let stateCP = Object.assign({}, this.state);
      stateCP.showCreateNewExamineeWindowModal = false;
      stateCP.newExamineeData = null;
      this.setState(stateCP);
    };
    handlers.modalDiscardClickHandler = (event) => {
      let stateCP = Object.assign({}, this.state);
      stateCP.newExamineeData = null;
      stateCP.showCreateNewExamineeWindowModal = false;
      this.setState(stateCP);
    }

    if (isUpdateForm) {
      handlers.modalSubmitClickHandler = (event) => {
        event.preventDefault();
        console.log('update');
      }
    } else {
      handlers.modalSubmitClickHandler = (event) => {
        event.preventDefault();
        this.createNewExamineeRequest();
      }
    }
    return handlers;
  }

  createNewExamineeRequest() {
    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerExamineeActions.createExamineeWithReferrer(this.state.newExamineeData)
      .then(test => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.showCreateNewExamineeWindowModal = false;
        stateCP.newExamineeData = null;
        this.loadExamineesInRelationship(stateCP);
        //this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        //this.setState(stateCP);
        this.loadExamineesInRelationship(stateCP);
      });
  }

  createNewExamineeData(data, setStateLater) {
    console.dir(data);
    let stateCP = Object.assign({}, this.state);
    stateCP.newExamineeData = Object.assign({
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      role: 'examinee',
      referrer: this.state.user.user['_id'],
      referrals: [],
      relationship: [{user: this.state.user.user['_id'], type: 'examiner'}]
    }, data || {});

    stateCP.showCreateNewExamineeWindowModal = true;
    if (setStateLater) {
      return stateCP;
    }
    this.setState(stateCP);
  }

  updateNewExamineeData(newExamineeData) {
      let stateCP = Object.assign({}, this.state);
      stateCP.newExamineeData = newExamineeData;
      this.setState(stateCP);
  }

  render() {
    return(
      <ExaminerTemplateComponent child={(
        <Fragment>
          <div>
            <h1>Examinees</h1>

            <ModalWindowComponent {...(this.modalWindowAttributes(this.state.isUpdateForm))}
                                show={this.state.showCreateNewExamineeWindowModal}
                                submitButtonText={this.state.isUpdateForm? 'Update' : 'Create'}
                                discardButtonText={this.state.isUpdateForm? 'discard' : 'Cancel'}
                                modalBody={(
                                          <ExaminerExamineeFormComponent {...this.state} updateNewExamineeData={(data) => {this.updateNewExamineeData(data)}}/>
                                        )}
               />

             <button type="button" onClick={(event) => this.createNewExamineeData()}>New Examinee</button>
            <ExamineesListComponent examinees={this.state.myExaminees}/>
          </div>
        </Fragment>
      )}/>
    );
  }
}

class ExamineesListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };

  }

  render() {
    return(
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Create Date</th>
            <th>Acion</th>
          </tr>
        </thead>
        <tbody>

          {
            this.props.examinees && this.props.examinees.map((examinee, index) => {
              return(
                <tr key={index}>
                  <td>{`${examinee.firstName} ${examinee.lastName}`}</td>
                  <td>{examinee.email}</td>
                  <td>{examinee.createdAt}</td>
                  <td>
                    <button type="button" >Remove</button>
                    <button type="button" >Send Exam</button>
                  </td>
                </tr>
              )
            })
          }

        </tbody>
      </table>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExaminerExamineesComponent)
