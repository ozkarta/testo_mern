import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import './index.css';
class ExaminerExamineeFormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
  }

  inputChangeHandler(value, field) {
    let stateCP = Object.assign({}, this.state);
    stateCP.newExamineeData[field] =  value;
    this.props.updateNewExamineeData(stateCP.newExamineeData);
    //this.setState(stateCP);
  }

  render() {
    return(
      <Fragment>
        <div className="form-group">
          <label htmlFor="fName">First Name:</label>
          <input type="text" className="form-control" id="fName"
            value={this.props.newExamineeData.firstName} onChange={(event) => {this.inputChangeHandler(event.target.value, 'firstName')}}/>
        </div>

        <div className="form-group">
          <label htmlFor="lName">Last Name:</label>
          <input type="text" className="form-control" id="lName"
            value={this.props.newExamineeData.lastName} onChange={(event) => {this.inputChangeHandler(event.target.value, 'lastName')}}/>
        </div>

        <div className="form-group">
          <label htmlFor="email">Email address:</label>
          <input type="email" className="form-control" id="email"
            value={this.props.newExamineeData.email} onChange={(event) => {this.inputChangeHandler(event.target.value, 'email')}}/>
        </div>

        <div className="form-group">
          <label htmlFor="pwd">Password:</label>
          <input type="password" className="form-control" id="pwd"
            value={this.props.newExamineeData.password} onChange={(event) => {this.inputChangeHandler(event.target.value, 'password')}}/>
        </div>

        <div className="form-group">
          <label htmlFor="rpwd">Repeat Password:</label>
          <input type="password" className="form-control" id="rpwd"/>
        </div>
      </Fragment>
    );
  }
}
export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExaminerExamineeFormComponent)
