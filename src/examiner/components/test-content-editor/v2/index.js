import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../../template';
import * as ExaminerTestActions from '../../actions/tests-v2.action';
import {EditorState, convertToRaw, ContentState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './index.css';

class TestContentEditorComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return(
      <ExaminerTemplateComponent child={
        (
          <Fragment>
            <br/><br/>
            <TestContentLayout {...this.state}/>
          </Fragment>
        )
      }/>
    );
  }
}


class TestContentLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
    this.createNewTestGroupObject = this.createNewTestGroupObject.bind(this);
    this.updateTestClickHandler = this.updateTestClickHandler.bind(this);
  }

  componentDidMount() {
    let stateCP = Object.assign({}, this.state);

    if (this.state && this.state.match && this.state.match.params && this.state.match.params.testId) {
      let testId = this.state.match.params.testId;
      stateCP.isUpdateForm = true;

      this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
      ExaminerTestActions.getTestById(testId)
        .then(test => {
          this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
          if (!test) {
            stateCP.testData = {};
          } else {
            if (!test.partialStateQuestionQuantity) {
              test.partialStateQuestionQuantity = '';
            }
            stateCP.testData = test;
          }

          this.setState(stateCP);
        })
        .catch(error => {
          this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
          stateCP.error = {
            msg: error.msg,
            visible: true
          };
          this.setState(stateCP);
        });
      return;
    }
  }

  createNewTestGroupObject() {
    let stateCP = Object.assign({}, this.state);
    let testGroup = {
      groupTitle: '',
      groupIntroduction: '',
      groupQuestionGeneralCondition: '',
      groupItems: [],
      open: true
    };
    stateCP.testData.testGroups.push(testGroup);
    this.setState(stateCP);
  }

  updateTestClickHandler(event) {
    event.preventDefault();

    let stateCP = Object.assign({}, this.state);
    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerTestActions.updateTest(this.state.testData)
      .then(updatedTestData => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        if (updatedTestData) {
          stateCP.testData = updatedTestData;
          this.setState(stateCP);
        }
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  render() {
    if (!this.state.testData) {
      return null;
    }
    return(
      <Fragment>

        <div className="card">
          <div className="card-header">
            <ul className="nav nav-pills card-header-pills">
              <li className="nav-item">
                <button type="button" onClick={this.createNewTestGroupObject}>New Test Group</button>
              </li>

              <li className="nav-item">
                <button type="button" onClick={this.updateTestClickHandler}>Update Test</button>
              </li>
            </ul>
          </div>

          {
            this.state.testData && this.state.testData.testGroups.map((testGroup, index) => {
              return(
                <Fragment key={index}>
                  <p>
                    <button className="btn btn-link" type="button"
                      aria-expanded={!testGroup.groupTitle}
                      data-toggle="collapse"
                      data-target={`#colapse_${index}`}
                      aria-controls={`colapse_${index}`}
                      onClick={(event) => {
                        let stateCP = Object.assign({}, this.state);
                        let tg = Object.assign({}, testGroup);
                        tg.open = !tg.open;
                        stateCP.testData.testGroups.splice(index, 1, tg);
                        this.setState(stateCP);
                      }}>
                        {testGroup.groupTitle || 'New Test Group'}
                    </button>
                  </p>
                  <div className={'collapse' + (testGroup.open? ' show': '')} id={`colapse_${index}`}>
                    <div className="card card-body">
                      <GroupComponent testGroup={testGroup} testData={this.state.testData}
                        testGroupChangeHandler={(updatedTestGroup) => {
                          let stateCP = Object.assign({}, this.state);
                          stateCP.testData.testGroups.splice(index, 1, updatedTestGroup);
                          this.setState(stateCP);
                        }}
                        testGroupRemoveHandler={() => {
                          let stateCP = Object.assign({}, this.state);
                          stateCP.testData.testGroups.splice(index, 1);
                          this.setState(stateCP);
                        }}/>
                    </div>
                  </div>
                </Fragment>
              )
            })
          }
        </div>

      </Fragment>
    );
  }
}

class GroupComponent extends Component {
  constructor(props) {
    super(props);
    let groupIntroductionEditorState;
    let groupQuestionGeneralConditionEditorState;
    // Restore from HTML
    if (props.testGroup.groupIntroduction) {
      const contentBlock = htmlToDraft(props.testGroup.groupIntroduction)
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks)
        groupIntroductionEditorState = EditorState.createWithContent(contentState)
      } else {
        groupIntroductionEditorState = EditorState.createEmpty()
      }
    } else {
      groupIntroductionEditorState = EditorState.createEmpty()
    }

    if (props.testGroup.groupQuestionGeneralCondition) {
      const contentBlock = htmlToDraft(props.testGroup.groupQuestionGeneralCondition)
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks)
        groupQuestionGeneralConditionEditorState = EditorState.createWithContent(contentState)
      } else {
        groupQuestionGeneralConditionEditorState = EditorState.createEmpty()
      }
    } else {
      groupQuestionGeneralConditionEditorState = EditorState.createEmpty()
    }
    // Set State
    this.state = {
      ...props,
      groupIntroduction: groupIntroductionEditorState,
      groupQuestionGeneralCondition: groupQuestionGeneralConditionEditorState
    }

    this.uploadImageCallBack = this.uploadImageCallBack.bind(this);
    this.newGroupItemClickHandler = this.newGroupItemClickHandler.bind(this);
    this.groupQuestionRemoveHandler = this.groupQuestionRemoveHandler.bind(this);
    this.newAnswerClickHandler = this.newAnswerClickHandler.bind(this);
    this.groupItemChangeHandler = this.groupItemChangeHandler.bind(this);
    this.defaultChangeHandler = this.defaultChangeHandler.bind(this);
  }

  onEditorStateChange(editorState, field) {
    let stateCP = Object.assign({}, this.state);
    stateCP[field] = editorState;
    stateCP.testGroup[field] = draftToHtml(convertToRaw(stateCP[field].getCurrentContent()));
    stateCP.testGroup.open = this.props.testGroup.open;
    this.props.testGroupChangeHandler(stateCP.testGroup);
    this.setState(stateCP);
  };

  uploadImageCallBack() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({data: {link:"https://i.imgur.com/hOxdW8S.png"}})
      }, 4000);

    })
  }

  newGroupItemClickHandler(type) {

    let groupItemObject = {
      type: type,
      question: '',
      answers: [],
      point: 1,
      timeForAnswering: 60,
      correctionOffers: [],
      disabled: false
    }

    if (this.props.testData.testProcessType === 'partial') {
      groupItemObject.timeForAnswering = this.props.testData.defaultTimeForAnswering || 0;
    } else {
        groupItemObject.timeForAnswering = 0;
    }

    let stateCP = Object.assign({}, this.state);
    if (!stateCP.testGroup.groupItems) {
      stateCP.testGroup.groupItems = [];
    }

    stateCP.testGroup.groupItems.push(groupItemObject);
    this.setState(stateCP);
  }

  groupQuestionRemoveHandler(groupItemIndex, answerIndex) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testGroup.groupItems[groupItemIndex].answers.splice(answerIndex, 1);
    this.setState(stateCP);
  }

  newAnswerClickHandler(groupItemIndex) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testGroup.groupItems[groupItemIndex].answers.push({correct: false, answerText: ''});
    this.setState(stateCP);
  }

  groupItemChangeHandler(groupItemIndex, updatedTestItem) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testGroup.groupItems.splice(groupItemIndex, 1, updatedTestItem);
    this.setState(stateCP);
  }

  groupItemRemoveHandler(groupItemIndex) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testGroup.groupItems.splice(groupItemIndex, 1);
    this.setState(stateCP);
  }

  defaultChangeHandler(field, value) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testGroup[field] =  value;
    // Updates parent's state, Not current
    //this.props.testGroupChangeHandler(stateCP.testGroup);
    this.setState(stateCP);
  }

  render() {
    return(
      <Fragment>
        <div className="card">
          <div className="card-header">

            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text" id="groupTitle">Group Title</span>
              </div>
              <input type="text" className="form-control" placeholder="Group Title" aria-label="groupTitle" aria-describedby="groupTitle"
                value={this.state.testGroup.groupTitle}
                onChange={(event) => {this.defaultChangeHandler('groupTitle', event.target.value)}}/>

                <input className="remove-button-x" type="image" src="/img/red_X.png" alt="remove" onClick={(event) => {
                  event.preventDefault();
                  this.props.testGroupRemoveHandler();
                }}/>
            </div>

          </div>
          <div className="card-body">
            <h2 className="card-title">Group Introduction</h2>
              <Editor
                editorState={this.state.groupIntroduction}
                wrapperClassName="demo-wrapper"
                editorClassName="demo-editor"
                toolbar={{
                  image: { uploadCallback: this.uploadImageCallBack, alt: { present: true, mandatory: true } },
                }}
                onEditorStateChange={(editorState) => {this.onEditorStateChange(editorState, 'groupIntroduction')}}
              />

            <h2 className="card-text">General Group Question.</h2>
              <Editor
                editorState={this.state.groupQuestionGeneralCondition}
                wrapperClassName="demo-wrapper"
                editorClassName="demo-editor"
                onEditorStateChange={(editorState) => {this.onEditorStateChange(editorState, 'groupQuestionGeneralCondition')}}
              />
          </div>
          <hr/>

          <div>
            {
              this.state.testGroup && this.state.testGroup.groupItems && this.state.testGroup.groupItems.map((groupItem, groupItemIndex) => {
                return (
                  <div key={groupItemIndex}>
                    <GroupItemTestComponent
                      testData={this.state.testData}
                      groupItem={groupItem}
                      testQuestionRemoveHandler={(answerIndex) => {
                        this.groupQuestionRemoveHandler(groupItemIndex, answerIndex)
                      }}
                      newAnswerClickHandler={() => {
                        this.newAnswerClickHandler(groupItemIndex);
                      }}
                      groupItemChangeHandler={(updatedGroupItem) => {
                        this.groupItemChangeHandler(groupItemIndex, updatedGroupItem);
                      }}/>
                  </div>
                )
              })
            }
            <button type="button" onClick={() => this.newGroupItemClickHandler('test')}>New  Test Group Item</button>
            <button type="button" onClick={() => this.newGroupItemClickHandler('text')}>New Texxt Group Item</button>
          </div>

        </div>
      </Fragment>
    );
  }
}

class GroupItemTestComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }

    this.defaultChangeHandler = this.defaultChangeHandler.bind(this);
    this.answerChangeHandler = this.answerChangeHandler.bind(this);
  }

  defaultChangeHandler(field, value) {
    let stateCP = Object.assign({}, this.state);
    stateCP.groupItem[field] =  value;
    // Updates parent's state, Not current
    this.props.groupItemChangeHandler(stateCP.groupItem);
    //this.setState(stateCP);
  }

  answerChangeHandler(newAnswer, index) {
    let stateCP = Object.assign({}, this.state);
    stateCP.groupItem.answers.splice(index, 1, newAnswer);
    // Updates parent's state, Not current
    this.props.groupItemChangeHandler(stateCP.groupItem);
  }

  render() {
    return(
      <Fragment>
          <div>

            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">Question</span>
              </div>
              <textarea className="form-control" aria-label="With textarea" value={this.props.groupItem.question} onChange={(event) => {
                this.defaultChangeHandler('question', event.target.value);
              }}>
              </textarea>
            </div>

            {
              this.props.groupItem.type === 'test' &&

              <div>
                {
                  this.props.groupItem.answers.map((answer, index) => {
                    return(
                      <div className="form-inline" key={index}>
                        <input type="checkbox" className="form-control" checked={answer.correct}
                        onChange={(event) => {
                           let answerCP = Object.assign({}, answer);
                           answerCP.correct = event.target.checked;
                           this.answerChangeHandler(answerCP, index);
                        }}/>
                        <div className="input-group mb-3">

                          <input type="text" className="form-control" value={answer.answerText}
                          onChange={(event) => {
                            let answerCP = Object.assign({}, answer);
                            answerCP.answerText = event.target.value;
                            this.answerChangeHandler(answerCP, index);
                          }}/>

                          <div className="input-group-append">
                            <span className="input-group-text" id="basic-addon2">
                              <input className="remove-button-x" type="image" src="/img/red_X.png" alt="remove" onClick={(event) => {
                                event.preventDefault();
                                this.props.testQuestionRemoveHandler(index);
                              }}/>
                            </span>
                          </div>
                        </div>
                      </div>
                    )
                  })
                }

                <button type="button" onClick={this.props.newAnswerClickHandler}>Add Answer</button>
              </div>
            }

            {
              this.props.testData && this.props.testData.testProcessType && this.props.testData.testProcessType === 'partial' &&

              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">Time For Answering (seconds)</span>
                </div>

                <div>
                  <input type="number" value={this.props.groupItem.timeForAnswering} onChange={(event) => this.defaultChangeHandler('timeForAnswering', event.target.value)} />
                </div>
              </div>
            }

          </div>
      </Fragment>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(TestContentEditorComponent)
