import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../../template';
import * as ExaminerTestActions from '../../actions/tests.action';
import './index.css';

class TestContentEditorComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return(
      <ExaminerTemplateComponent child={
        (
          <Fragment>
            <br/><br/>
            <TestContentLayour {...this.state}/>
          </Fragment>
        )
      }/>
    );
  }
}

class TestContentLayour extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
    this.testQuestionRemoveHandler = this.testQuestionRemoveHandler.bind(this);
    this.newTestItemClickHandler = this.newTestItemClickHandler.bind(this);
    this.testItemChangeHandler = this.testItemChangeHandler.bind(this);
    this.saveTestClickHandler = this.saveTestClickHandler.bind(this);
    this.RemoveTestItemClickHandler = this.RemoveTestItemClickHandler.bind(this);
    this.disableTestItemClickHandler = this.disableTestItemClickHandler.bind(this);
  }

  componentDidMount() {
    let stateCP = Object.assign({}, this.state);

    if (this.state && this.state.match && this.state.match.params && this.state.match.params.testId) {
      let testId = this.state.match.params.testId;
      stateCP.isUpdateForm = true;

      this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
      ExaminerTestActions.getTestById(testId)
        .then(test => {
          this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
          if (!test) {
            stateCP.testData = {};
          } else {
            if (!test.partialStateQuestionQuantity) {
              test.partialStateQuestionQuantity = '';
            }
            stateCP.testData = test;
          }

          this.setState(stateCP);
        })
        .catch(error => {
          this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
          stateCP.error = {
            msg: error.msg,
            visible: true
          };
          this.setState(stateCP);
        });
      return;
    }
  }

  testQuestionRemoveHandler(testItemIndex, answerIndex) {
    let stateCP =  Object.assign({}, this.state);
    stateCP.testData.testItems[testItemIndex].answers.splice(answerIndex, 1);
    this.setState(stateCP);
  }

  newTestItemClickHandler() {
    let testItemObject = {
      question: '',
      atachment: '',
      answers: [],
      multyAnswer: true,
      point: 1,
      defaultTimeForAnswering: 60, // seconds
      type: 'regular',
    }

    let stateCP = Object.assign({}, this.state);
    if (!stateCP.testData.testItems) {
      stateCP.testData.testItems = [];
    }

    stateCP.testData.testItems.push(testItemObject);
    this.setState(testItemObject);
  }

  newQuestionClickHandler(testItemIndex) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testData.testItems[testItemIndex].answers.push({correct: false, answerText: ''});
    this.setState(stateCP);
  }

  testItemChangeHandler(testItemIndex, testItem) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testData.testItems.splice(testItemIndex, 1, testItem);
    this.setState(stateCP);
  }

  saveTestClickHandler() {
    let stateCP = Object.assign({}, this.state);
    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerTestActions.updateTest(this.state.testData)
      .then(updatedTestData => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        if (updatedTestData) {
          stateCP.testData = updatedTestData;
          this.setState(stateCP);
        }
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  RemoveTestItemClickHandler(indexAt) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testData.testItems.splice(indexAt, 1);
    this.setState(stateCP);
  }

  disableTestItemClickHandler(indexAt, disabled) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testData.testItems[indexAt].disabled = disabled;
    this.setState(stateCP);
  }
  render() {
    return(
      <Fragment>
        <button type="button" onClick={this.newTestItemClickHandler}>New Test Item</button>
        <button type="button" onClick={this.saveTestClickHandler}>Save Test</button>
        {
          this.state.testData && this.state.testData.testItems && this.state.testData.testItems.map((testItem, testItemIndex) => {
            return(
              <Fragment key={testItemIndex}>
                <div className="inline-form">
                  <hr/>
                  <span>{testItemIndex + 1}</span>
                  <button type="button" onClick={() => {this.RemoveTestItemClickHandler(testItemIndex)}}>Remove Test Item</button>
                  <button type="button" onClick={() => {this.disableTestItemClickHandler(testItemIndex, !testItem.disabled)}}>{testItem.disabled? 'Enable': 'Disable'}</button>
                  <TestItemComponent testItem={testItem}
                                     testQuestionRemoveHandler={(answerIndex) => {
                                       this.testQuestionRemoveHandler(testItemIndex, answerIndex)
                                     }}
                                     newQuestionClickHandler={() => {
                                       this.newQuestionClickHandler(testItemIndex);
                                     }}
                                     testItemChangeHandler={(updatedTestItem) => {
                                       this.testItemChangeHandler(testItemIndex, updatedTestItem);
                                     }}/>
                  <hr/>
                </div>
              </Fragment>
            )
          })
        }

      </Fragment>
    );
  }
}

class TestItemComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }

    this.defaultChangeHandler = this.defaultChangeHandler.bind(this);
    this.answerChangeHandler = this.answerChangeHandler.bind(this);
  }

  defaultChangeHandler(field, value) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testItem[field] =  value;
    // Updates parent's state, Not current
    this.props.testItemChangeHandler(stateCP.testItem);
    //this.setState(stateCP);
  }

  answerChangeHandler(newAnswer, index) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testItem.answers.splice(index, 1, newAnswer);
    // Updates parent's state, Not current
    this.props.testItemChangeHandler(stateCP.testItem);
    //this.setState(stateCP);
  }

  render() {
    return(
      <Fragment>
          <div>

            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">Question</span>
              </div>
              <textarea className="form-control" aria-label="With textarea" value={this.props.testItem.question} onChange={(event) => {
                this.defaultChangeHandler('question', event.target.value);
              }}>
              </textarea>
            </div>

            {
              this.props.testItem.answers.map((answer, index) => {
                return(
                  <div className="form-inline" key={index}>
                    <input type="checkbox" className="form-control" checked={answer.correct}
                    onChange={(event) => {
                       let answerCP = Object.assign({}, answer);
                       answerCP.correct = event.target.checked;
                       this.answerChangeHandler(answerCP, index);
                    }}/>
                    <div className="input-group mb-3">

                      <input type="text" className="form-control" value={answer.answerText}
                      onChange={(event) => {
                        let answerCP = Object.assign({}, answer);
                        answerCP.answerText = event.target.value;
                        this.answerChangeHandler(answerCP, index);
                      }}/>

                      <div className="input-group-append">
                        <span className="input-group-text" id="basic-addon2">
                          <input className="remove-button-x" type="image" src="/img/red_X.png" alt="remove" onClick={(event) => {
                            event.preventDefault();
                            this.props.testQuestionRemoveHandler(index);
                          }}/>
                        </span>
                      </div>
                    </div>
                  </div>
                )
              })
            }

            <button type="button" onClick={this.props.newQuestionClickHandler}>Add Answer</button>
          </div>
      </Fragment>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(TestContentEditorComponent)


//              TEST DATA
// ,
// testData: {
//   partialStateQuestionQuantity: null,
//   defaultTimeForAnswering: 60,
//   contributors: [],
//   testItems: [
//     {
//     question: 'What\'s your name?',
//     atachment: '',
//     answers: [
//       {
//         answerText: 'Oz',
//         correct: false
//       },
//       {
//         answerText: 'Kart',
//         correct: true
//       },
//       {
//         answerText: 'Hamlet',
//         correct: false
//       },
//       {
//         answerText: 'Test',
//         correct: false
//       },
//       {
//         answerText: 'P',
//         correct: false
//       },
//     ],
//     multyAnswer: false,
//     point: 1,
//     defaultTimeForAnswering: 60,
//     type: 'regular',
//   },
//   {
//     question: 'What is this language',
//     atachment: '',
//     answers: [
//       {
//         answerText: 'English',
//         correct: false
//       },
//       {
//         answerText: 'NodeJS',
//         correct: true
//       },
//       {
//         answerText: 'Hamlet',
//         correct: false
//       },
//       {
//         answerText: 'GO',
//         correct: false
//       },
//       {
//         answerText: 'ReactJS',
//         correct: true
//       },
//       {
//         answerText: 'AngualarJS',
//         correct: false
//       }
//     ],
//     multyAnswer: false,
//     point: 1,
//     defaultTimeForAnswering: 60,
//     type: 'regular',
//   }],
//   _id: "5b1ec78ae8545e63cf1522b2",
//   title: "test data mobile test",
//   friendlyId: "test-data-mobile-test",
//   type: "regular",
//   state: "full",
//   audiance: "private",
//   status: "pending",
//   owner: "5b1b7f2d1a8d740014e853a4",
//   createdAt: "2018-06-11T19:03:38.696Z",
//   updatedAt: "2018-06-11T19:03:38.696Z"
//   }
