import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import ErrorAlertComponent from '../../../../shared/components/error-alert';
import './tests.form.css';

class TestsFormComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ...props
    }

    this.inputChangeHandler = this.inputChangeHandler.bind(this);
  }

  inputChangeHandler(value, field, cb) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testData[field] =  value;
    this.setState(stateCP);

    if (cb && typeof cb === 'function') {
      return cb();
    }

    this.props.updateParentState(stateCP.testData);
  }

  generateFriendlyId(stringValue) {
    let result = stringValue.toLowerCase();
    // replace characters with special symbol
    result = result.replace(/ |\.|,|\/|\|\\|\]|\}|\[|\{|\)|\(|\+|=|_|\*|&|\^|%|\$|#|@|\|!|~|`|:|;|"|'|<|>|\?|\\/g, '-');
    return result;
  }

  render() {
    return(
      <Fragment>
        <br/><br/>
        <h1>Tests Form</h1>


          <form>

            <ErrorAlertComponent {...this.state.error}/>
            <div className="form-group">
              <label htmlFor="title">Test Title</label>
              <input type="text" className="form-control" id="title"
                value={this.state.testData.title}
                onChange={(event) => {
                  this.inputChangeHandler(event.target.value, 'title', () => {
                    this.inputChangeHandler(this.generateFriendlyId(event.target.value), 'friendlyId');
                  })
                }}/>
            </div>

            <div className="form-group">
              <label htmlFor="friendlyId">Friendly ID</label>
              <input type="text" className="form-control" id="friendlyId"
                value={this.state.testData.friendlyId}
                onChange={(event) => {
                  this.inputChangeHandler(this.generateFriendlyId(event.target.value), 'friendlyId');
                }}/>
            </div>

            <div className="form-group">
              <label htmlFor="state">Test State</label>
              <select defaultValue={this.state.testData.testProcessType} className="custom-select" id="testProcessType"
                onChange={(event) => {
                  this.inputChangeHandler(event.target.value, 'testProcessType', () => {
                    if (event.target.value !== 'partial') {
                      //this.inputChangeHandler('', 'partialStateQuestionQuantity');
                      this.inputChangeHandler('60', 'fullDuration');
                      this.inputChangeHandler('', 'defaultTimeForAnswering');
                    } else {
                      //this.inputChangeHandler(this.state.partialStateQuestionDefaultQuantity, 'partialStateQuestionQuantity');
                      this.inputChangeHandler('', 'fullDuration');
                      this.inputChangeHandler('60', 'defaultTimeForAnswering');
                    }
                  });
                }}>
                <option value="full">Full</option>
                <option value="partial">Partial</option>
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="defaultTimeForAnswering">Default Time For Answering question (seconds)</label>
              <input type="number" className="form-control" id="defaultTimeForAnswering"
                disabled={this.state.testData.testProcessType === 'full'}
                value={this.state.testData.defaultTimeForAnswering || 0}
                onChange={(event) => {
                  this.inputChangeHandler(event.target.value, 'defaultTimeForAnswering');
                }}/>
            </div>

            <div className="form-group">
              <label htmlFor="fullDuration">Test Duration (minutes)</label>
              <input type="number" className="form-control" id="fullDuration"
                disabled={this.state.testData.testProcessType === 'partial'}
                value={this.state.testData.fullDuration || 0}
                onChange={(event) => {
                  this.inputChangeHandler(event.target.value, 'fullDuration');
                }}/>
            </div>

          </form>
      </Fragment>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(TestsFormComponent)
