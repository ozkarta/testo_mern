import React, {Component, Fragment} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../../template';
import * as ExaminerTestActions from '../../actions/tests-v2.action';
import ModalWindowComponent from '../../../../shared/components/modal-window';
import TestsFormComponent from './tests.form';

import './index.css';
class ExaminerTestsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,

      // _____________________
      showCreateNewTestWindowModal: false
    };

    this.createNewTest = this.createNewTest.bind(this);
    this.modalWindowAttributes = this.modalWindowAttributes.bind(this);
    this.updateStateTestData = this.updateStateTestData.bind(this);
    this.testListComponentHandlers = this.testListComponentHandlers.bind(this);
  }

  componentDidMount() {
    let userId = this.state.user.user['_id'];
    this.requestExaminerTests(userId);
  }

  requestExaminerTests(examinerId) {
    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerTestActions.requestExaminerTests(examinerId)
      .then(tests => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.tests = tests || [];
        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  deleteTest(testId) {
    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerTestActions.deleteTest(testId)
      .then(success => {
        let userId = this.state.user.user['_id'];
        this.requestExaminerTests(userId);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  createNewTest() {
    let stateCP = Object.assign({}, this.state);
    stateCP.newTestData = this.createNewTestObject();
    stateCP.isUpdateForm = false;
    stateCP.showCreateNewTestWindowModal = true;
    this.setState(stateCP);
  }

  createNewTestObject() {
    if (this.state.user && this.state.user.user && this.state.user.user['_id']) {
      return {
        title: '',
        friendlyId: '',
        testProcessType: 'full',  // full or partial
        partialStateQuestionQuantity: '',
        defaultTimeForAnswering: 0,
        fullDuration: 60,
        testItems: [],
        owner: this.state.user.user['_id']
      };
    } else {
      let stateCP = Object.assign({}, this.state);
      stateCP.redirectTo = '/';
      this.setState(stateCP);
    }

  }

  updateStateTestData(updatedTestData) {
    let stateCP = Object.assign({}, this.state);
    stateCP.newTestData = updatedTestData;
    this.setState(stateCP);
  }

  modalWindowAttributes(isUpdateForm) {
    let handlers = {};
    handlers.modalExitClickHandler = (event) => {
      let stateCP = Object.assign({}, this.state);
      stateCP.showCreateNewTestWindowModal = false;
      stateCP.newTestData = null;
      this.setState(stateCP);
    };
    handlers.modalDiscardClickHandler = (event) => {
      let stateCP = Object.assign({}, this.state);
      stateCP.newTestData = null;
      stateCP.showCreateNewTestWindowModal = false;
      this.setState(stateCP);
    }

    if (isUpdateForm) {
      handlers.modalSubmitClickHandler = (event) => {
        event.preventDefault();
        let stateCP = Object.assign({}, this.state);
        stateCP.error = {
          msg: this.state.error?this.state.error.msg : '',
          visible: false
        };

        this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
        ExaminerTestActions.updateTest(this.state.newTestData)
          .then(test => {
            stateCP.showCreateNewTestWindowModal = false;
            stateCP.newTestData = null;
            this.setState(stateCP);
            let userId = this.state.user.user['_id'];
            this.requestExaminerTests(userId);
          })
          .catch(error => {
            this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
            stateCP.error = {
              msg: error.msg,
              visible: true
            };
            this.setState(stateCP);
          });
      }
    } else {
      handlers.modalSubmitClickHandler = (event) => {
        event.preventDefault();
        let stateCP = Object.assign({}, this.state);
        stateCP.error = {
          msg: this.state.error?this.state.error.msg : '',
          visible: false
        };

        this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
        ExaminerTestActions.createTest(this.state.newTestData)
          .then(test => {
            stateCP.showCreateNewTestWindowModal = false;
            stateCP.newTestData = null;
            this.setState(stateCP);
            let userId = this.state.user.user['_id'];
            this.requestExaminerTests(userId);
          })
          .catch(error => {
            this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
            stateCP.error = {
              msg: error.msg,
              visible: true
            };
            this.setState(stateCP);
          });
      }
    }
    return handlers;
  }

  testListComponentHandlers() {
    return {
      testDeleteClickHandler: (testId) => {
        this.deleteTest(testId);
      },
      testEditClickHandler: (test) => {
        let stateCP = Object.assign({}, this.state);
        stateCP.newTestData = Object.assign({}, test);
        stateCP.isUpdateForm = true;
        stateCP.showCreateNewTestWindowModal = true;
        this.setState(stateCP);
      }
    }
  }

  render() {
    return(
      <ExaminerTemplateComponent child={
        (
          <Fragment>
            <div>
              <br/><br/>
              <ModalWindowComponent {...(this.modalWindowAttributes(this.state.isUpdateForm))}
                                    show={this.state.showCreateNewTestWindowModal}
                                    submitButtonText={this.state.isUpdateForm? 'Update' : 'Create'}
                                    discardButtonText={this.state.isUpdateForm? 'discard' : 'Cancel'}
                                    modalBody={(<TestsFormComponent
                                                    testData={this.state.newTestData}
                                                    partialStateQuestionDefaultQuantity={10}
                                                    updateParentState={this.updateStateTestData}
                                                  />
                                            )}
                            />
              <button type="button" onClick={this.createNewTest}>New Test</button>
              <TestListComponent {...this.state.tests} {...(this.testListComponentHandlers())}/>
            </div>
          </Fragment>
        )
      }/>
    );
  }
}

class TestListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
    this.calculateTestQTY = this.calculateTestQTY.bind(this);
  }

  calculateTestQTY(testGroups) {

    if (!testGroups.length) {
      return 0;
    }

    return testGroups.reduce((accumulator, testGroup) => {
      if (!testGroup || !testGroup.groupItems || !testGroup.groupItems.length) {
        return accumulator;
      }
      return accumulator + testGroup.groupItems.length;
    },0);

  }

  render() {
    return(
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Create Date</th>
            <th scope="col">Title</th>
            <th scope="col">Test Type</th>
            <th scope="col">Partial Question QTY</th>
            <th scope="col">Test Items</th>
            <th scope="col">Default Time For Answering(seconds)</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {
            this.props && this.props.tests && this.props.tests.map((test, index) => {
              return (
                <tr key={index}>
                  <th scope="row">{index + 1}</th>
                  <td>{test.createdAt}</td>
                  <td>{test.title}</td>
                  <td>{test.testProcessType === 'full' ? 'Full' : 'Partial'}</td>
                  <td>{test.testProcessType === 'partial' ? test.partialStateQuestionQuantity : 'N/A'}</td>
                  <td>{this.calculateTestQTY(test.testGroups)}</td>
                  <td>{test.defaultTimeForAnswering}</td>
                  <td>
                    <div className="row">
                      <button type="button" onClick={(event) => {
                          event.preventDefault();
                          this.props.testEditClickHandler(test);
                        }}>
                        Edit
                      </button>
                      <Link to={`/examiner/tests/${test['_id']}/content`}>Content</Link>
                      <button type="button" onClick={(event) => {
                          event.preventDefault();
                          this.props.testDeleteClickHandler(test['_id']);
                        }}>
                        Delete
                      </button>
                    </div>
                  </td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExaminerTestsComponent)
