import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import ExaminerTemplateComponent from '../../template';
import ErrorAlertComponent from '../../../../shared/components/error-alert';

import * as ExaminerTestActions from '../../actions/tests.action';
import './tests.form.css';

class TestsFormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return(
      <ExaminerTemplateComponent child={
        (
          <Fragment>
            <br/><br/>
            <h1>Tests Form</h1>
            <TestFormItemComponent {...this.state}/>
          </Fragment>
        )
      }/>
    );
  }
}

class TestFormItemComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      partialStateQuestionDefaultQuantity: 10,
      redirectTo: '',
      error: {
        msg: '',
        visible: false
      },
      testData: {
        title: '',
        friendlyId: '',
        type: 'regular',
        state: 'full',
        audiance: 'private',
        status: 'pending',
        partialStateQuestionQuantity: '',
        defaultTimeForAnswering: 60, // seconds
        testItems: [],
        owner: this.props.user.user['_id']
      }
    }
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.createFormSubmitEventHandler = this.createFormSubmitEventHandler.bind(this);
    this.updateFormSubmitEventHandler = this.updateFormSubmitEventHandler.bind(this);
  }

  componentDidMount() {
    let stateCP = Object.assign({}, this.state);

    if (this.state && this.state.match && this.state.match.params && this.state.match.params.testId) {
      let testId = this.state.match.params.testId;
      stateCP.isUpdateForm = true;

      this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
      ExaminerTestActions.getTestById(testId)
        .then(test => {
          this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
          if (!test) {
            stateCP.testData = {};
          } else {
            if (!test.partialStateQuestionQuantity) {
              test.partialStateQuestionQuantity = '';
            }
            stateCP.testData = test;
          }

          this.setState(stateCP);
        })
        .catch(error => {
          this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
          stateCP.error = {
            msg: error.msg,
            visible: true
          };
          this.setState(stateCP);
        });
      return;
    }
  }

  inputChangeHandler(value, field, cb) {
    let stateCP = Object.assign({}, this.state);
    stateCP.testData[field] =  value;
    this.setState(stateCP);

    if (cb && typeof cb === 'function') {
      cb();
    }
  }

  generateFriendlyId(stringValue) {
        let result = stringValue.toLowerCase();
        // replace characters with special symbol
        result = result.replace(/ |\.|,|\/|\|\\|\]|\}|\[|\{|\)|\(|\+|=|_|\*|&|\^|%|\$|#|@|\|!|~|`|:|;|"|'|<|>|\?|\\/g, '-');
        return result;
    }

  createFormSubmitEventHandler(event) {
    event.preventDefault();
    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerTestActions.createTest(this.state.testData)
      .then(test => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.redirectTo = '/examiner/tests';
        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });

  }

  updateFormSubmitEventHandler(event) {
    event.preventDefault();
    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerTestActions.updateTest(this.state.testData)
      .then(test => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.redirectTo = '/examiner/tests';
        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  render() {
    if (this.state.redirectTo) {
      return (<Redirect to={this.state.redirectTo}></Redirect>);
    }

    if (!this.state.testData && this.state.isUpdateForm) {
      return(
        <ErrorAlertComponent {...{msg: 'Test you requested was not found', visible: true}}/>
      );
    }

    return(
      <form onSubmit={this.state.isUpdateForm? this.updateFormSubmitEventHandler : this.createFormSubmitEventHandler}>

        <ErrorAlertComponent {...this.state.error}/>
        <div className="form-group">
          <label htmlFor="title">Test Title</label>
          <input type="text" className="form-control" id="title"
            value={this.state.testData.title}
            onChange={(event) => {
              this.inputChangeHandler(event.target.value, 'title', () => {
                this.inputChangeHandler(this.generateFriendlyId(event.target.value), 'friendlyId');
              })
            }}/>
        </div>

        <div className="form-group">
          <label htmlFor="friendlyId">Friendly ID</label>
          <input type="text" className="form-control" id="friendlyId"
            value={this.state.testData.friendlyId}
            onChange={(event) => {
              this.inputChangeHandler(this.generateFriendlyId(event.target.value), 'friendlyId');
            }}/>
        </div>

        <div className="container">
          <div className="row">

            <div className="col">
              <div className="form-group">
                <label htmlFor="type">Test Type</label>
                <select defaultValue={this.state.testData.type} className="custom-select" id="type"
                  onChange={(event) => {
                    this.inputChangeHandler(this.generateFriendlyId(event.target.value), 'type');
                  }}>
                  <option value="regular">Regular</option>
                  <option value="mixed">Mixed</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="state">Test State</label>
                <select defaultValue={this.state.testData.state} className="custom-select" id="state"
                  onChange={(event) => {
                    this.inputChangeHandler(this.generateFriendlyId(event.target.value), 'state', () => {
                      if (event.target.value !== 'partial') {
                        this.inputChangeHandler('', 'partialStateQuestionQuantity');
                      } else {
                        this.inputChangeHandler(this.state.partialStateQuestionDefaultQuantity, 'partialStateQuestionQuantity');
                      }
                    });
                  }}>
                  <option value="full">Full</option>
                  <option value="partial">Partial</option>
                </select>
              </div>
            </div>

            <div className="col">
              <div className="form-group">
                <label htmlFor="audiance">Audiance</label>
                <select defaultValue={this.state.testData.audiance} className="custom-select" id="audiance"
                  onChange={(event) => {
                    this.inputChangeHandler(this.generateFriendlyId(event.target.value), 'audiance');
                  }}>
                  <option value="public">Public</option>
                  <option value="private">Private</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="status">Status</label>
                <select defaultValue={this.state.testData.status} className="custom-select" id="status"
                  onChange={(event) => {
                    this.inputChangeHandler(this.generateFriendlyId(event.target.value), 'status');
                  }}>
                  <option value="published">Published</option>
                  <option value="pending">Pending</option>
                </select>
              </div>
            </div>

          </div>
        </div>

        <div className="form-group">
          <label htmlFor="partialStateQuestionQuantity">State Question Quantity (Partial State Only)</label>
          <input type="number" className="form-control" id="partialStateQuestionQuantity" disabled={this.state.testData.state !== 'partial'}
            value={this.state.testData.partialStateQuestionQuantity}
            onChange={(event) => {
              this.inputChangeHandler(event.target.value, 'partialStateQuestionQuantity');
            }}/>
        </div>

        <div className="form-group">
          <label htmlFor="defaultTimeForAnswering">Default Time For Answering question</label>
          <input type="number" className="form-control" id="defaultTimeForAnswering"
            value={this.state.testData.defaultTimeForAnswering}
            onChange={(event) => {
              this.inputChangeHandler(event.target.value, 'defaultTimeForAnswering');
            }}/>
        </div>

        <button type="submit" className="btn btn-primary form-control">{this.state.isUpdateForm? 'Update' : 'Create'}</button>
      </form>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(TestsFormComponent)
