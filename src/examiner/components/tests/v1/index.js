import React, {Component, Fragment} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../../template';
import * as ExaminerTestActions from '../../actions/tests.action';

import './index.css';
class ExaminerTestsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  componentDidMount() {
    let userId = this.state.user.user['_id'];
    this.requestExaminerTests(userId);
  }

  requestExaminerTests(examinerId) {
    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerTestActions.requestExaminerTests(examinerId)
      .then(tests => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.tests = tests || [];
        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  render() {
    return(
      <ExaminerTemplateComponent child={
        (
          <Fragment>
            <br/><br/>
            <Link to='/examiner/tests/new'>Add new test</Link>
            <TestsTableComponent {...this.state.tests}/>
          </Fragment>
        )
      }/>
    );
  }
}


class TestsTableComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  render() {
    return(
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Create Date</th>
            <th scope="col">Title</th>
            <th scope="col">Type</th>
            <th scope="col">Status</th>
            <th scope="col">Test Items</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {
            this.props && this.props.tests && this.props.tests.map((test, index) => {
              return (
                <tr key={index}>
                  <th scope="row">{index + 1}</th>
                  <td>{test.createdAt}</td>
                  <td>{test.title}</td>
                  <td>{test.type === 'regular' ? 'Regular' : 'Mixed'}</td>
                  <td>{test.status === 'pending'? 'Pending' : 'Published'}</td>
                  <td>{test.testItems.length || 0}</td>
                  <td>
                    <div className="row">
                      <Link to={`/examiner/tests/edit/${test['_id']}`}>Edit</Link> /
                      <Link to={`/examiner/tests/${test['_id']}/content`}>Content</Link> /
                      <button type="button">Delete</button>
                    </div>
                  </td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
    );
  }
}
export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExaminerTestsComponent)
