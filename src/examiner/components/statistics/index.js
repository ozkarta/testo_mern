import React, {Component} from 'react';
import ExaminerTemplateComponent from '../template';

import './index.css';
export default class ExaminerStatisticsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  render() {
    return(
      <ExaminerTemplateComponent child={(<h1>Examiner Statistics</h1>)}/>
    );
  }
}
