import axios from 'axios';

const apiBaseUrl = '/api/v1';

export async function createExamOffer(offer) {
    try {
        const response =  await axios.post(`${apiBaseUrl}/examiner/exam-offer`, offer);
        return (response && response.data) ? response.data : null;
    } catch (ex) {
        throw ex.response.data;
    }
}
