import axios from 'axios';

const apiBaseUrl = '/api/v1';

export async function createTest(test) {
    try {
        const response =  await axios.post(`${apiBaseUrl}/examiner/test`, test);
        return (response && response.data) ? response.data : null;
    } catch (ex) {
        throw ex.response.data;
    }
}

export async function updateTest(test) {
  try {
      const response =  await axios.put(`${apiBaseUrl}/examiner/test`, test);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}

export async function requestExaminerTests(examinerId) {
  try {
      const response =  await axios.get(`${apiBaseUrl}/examiner/test/owner/${examinerId}`);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}

export async function getTestById(testId) {
  try {
      const response =  await axios.get(`${apiBaseUrl}/examiner/test/item/${testId}`);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}
