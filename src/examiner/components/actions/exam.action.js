import axios from 'axios';

const apiBaseUrl = '/api/v1';

export async function createExam(exam) {
    try {
        const response =  await axios.post(`${apiBaseUrl}/examiner/exam`, exam);
        return (response && response.data) ? response.data : null;
    } catch (ex) {
        throw ex.response.data;
    }
}

export async function updateExam(exam) {
    try {
        const response =  await axios.put(`${apiBaseUrl}/examiner/exam`, exam);
        return (response && response.data) ? response.data : null;
    } catch (ex) {
        throw ex.response.data;
    }
}

export async function getExams(examinerId, searchTerm) {
  try {
      const response =  await axios.get(`${apiBaseUrl}/examiner/exam/owner/${examinerId}${searchTerm? ('?searchTerm='+searchTerm): ''}`);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}

export async function deleteExam(examId) {
  try {
      const response =  await axios.delete(`${apiBaseUrl}/examiner/exam/${examId}`);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}
