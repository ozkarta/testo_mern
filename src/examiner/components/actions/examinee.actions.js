import axios from 'axios';

const apiBaseUrl = '/api/v1';

export async function createExamineeWithReferrer(examineeUserData) {
    try {
        const response =  await axios.post(`${apiBaseUrl}/examiner/examinee/create-examinee`, examineeUserData);
        return (response && response.data) ? response.data : null;
    } catch (ex) {
        throw ex.response.data;
    }
}

export async function getExamineeRelationships(userId) {
    try {
        const response =  await axios.get(`${apiBaseUrl}/examiner/examinee/in-relationship/${userId}`);
        return (response && response.data) ? response.data : null;
    } catch (ex) {
        throw ex.response.data;
    }
}

export async function getExamineeByEmail(email) {
    try {
        const response =  await axios.get(`${apiBaseUrl}/examiner/examinee/by-email${email? (`?email=${email}`) : ''}`);
        return (response && response.data) ? response.data : null;
    } catch (ex) {
        throw ex.response.data;
    }
}
