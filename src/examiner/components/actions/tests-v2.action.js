import axios from 'axios';

const apiBaseUrl = '/api/v1';

export async function createTest(test) {
    try {
        const response =  await axios.post(`${apiBaseUrl}/examiner/test-v2`, test);
        return (response && response.data) ? response.data : null;
    } catch (ex) {
        throw ex.response.data;
    }
}

export async function updateTest(test) {
  try {
      const response =  await axios.put(`${apiBaseUrl}/examiner/test-v2`, test);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}

export async function deleteTest(testId) {
  try {
      const response =  await axios.delete(`${apiBaseUrl}/examiner/test-v2/${testId}`);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}

export async function requestExaminerTests(examinerId, searchTerm) {
  try {
      const response =  await axios.get(`${apiBaseUrl}/examiner/test-v2/owner/${examinerId}${searchTerm? ('?searchTerm='+searchTerm): ''}`);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}

export async function getTestById(testId) {
  try {
      const response =  await axios.get(`${apiBaseUrl}/examiner/test-v2/item/${testId}`);
      return (response && response.data) ? response.data : null;
  } catch (ex) {
      throw ex.response.data;
  }
}
