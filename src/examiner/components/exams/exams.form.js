import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import * as ExaminerTestActions from '../actions/tests-v2.action';
import {AsyncTypeahead} from 'react-bootstrap-typeahead';
import './exams.form.css';

class ExamsFormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };

    this.generatePassword = this.generatePassword.bind(this);
    this.defaultChangeHandler = this.defaultChangeHandler.bind(this);
  }

  componentDidMount() {
    let stateCP = Object.assign({}, this.state);
    let testId = this.props.newExamData.test? (this.props.newExamData.test['_id'] || this.props.newExamData.test) : '';
    if (!testId) {
      return;
    }

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerTestActions.getTestById(testId)
      .then(test => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        if (!test) {
          stateCP.testData = {};
        } else {
          if (!test.partialStateQuestionQuantity) {
            test.partialStateQuestionQuantity = '';
          }
          stateCP.newExamData.test = test;
        }

        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  generatePassword() {
    let password = Math.random().toString(36).slice(-8);
    let stateCP = Object.assign({}, this.state);
    stateCP.newExamData.password = password;
    this.setState(stateCP);
  }

  defaultChangeHandler(value, field, cb) {
    let stateCP = Object.assign({}, this.state);
    stateCP.newExamData[field] =  value;
    this.setState(stateCP);

    if (cb && typeof cb === 'function') {
      return cb();
    }

    this.props.updateParentState(stateCP.newExamData);
  }

  render() {
    return(
      <Fragment>

        <label htmlFor="title">Title</label>
        <div className="input-group mb-3">
          <input type="text" className="form-control" placeholder="Exam Title" aria-label="title" aria-describedby="title" name="title"
            value={this.state.newExamData.title}
            onChange={(event) =>{this.defaultChangeHandler(event.target.value, 'title')}}/>
        </div>

        <label>Test Title</label>
        <TypeaheadSearchComponent {...this.state}
          typeAheadChangeHandler={(resultId) => {
            this.defaultChangeHandler(resultId, 'test');
          }}/>

        <label htmlFor="password">Password</label>
        <div className="input-group mb-3">
          <input type="text" className="form-control" placeholder="Password" aria-label="password" aria-describedby="password" name="password"
            value={this.state.newExamData.password}
            onChange={(event) =>{this.defaultChangeHandler(event.target.value, 'password')}}/>
          <div className="input-group-append">
            <span className="input-group-text" id="basic-addon2">
              <button className="button" onClick={this.generatePassword}>Generate Password</button>
            </span>
          </div>
        </div>
      </Fragment>
    );
  }
}


class TypeaheadSearchComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      typeAheadOptions: {
        allowNew: false,
        isLoading: false,
        multiple: false,
        options: [],
      }
    };
  }

  _handleSearch = (searchTerm) => {
    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    let examinerId = this.state.user.user['_id'];
    stateCP.typeAheadOptions.isLoading = true;
    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerTestActions.requestExaminerTests(examinerId, searchTerm)
      .then(result => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.typeAheadOptions.isLoading = false;
        stateCP.typeAheadOptions.options = result.tests || [];
        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.typeAheadOptions.isLoading = false;
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  render() {
    const defaultSelected = (this.props.newExamData.test && typeof this.props.newExamData.test === 'object')? [this.props.newExamData.test] : [];
    return(
      <div>
      {
        this.props.newExamData && (typeof this.props.newExamData.test === 'object') &&
          <AsyncTypeahead
            {...this.state.typeAheadOptions}
            defaultSelected={defaultSelected}
            labelKey={(option) => {
              return option.title || option;
            }}
            minLength={1}
            onSearch={this._handleSearch}
            placeholder="Search for a tests"
            renderMenuItemChildren={(option, props) => (
              <h3>{option.title}</h3>
            )}
            onChange={(values) => {
              let value = values[0];
              if (typeof value === 'object' && value['_id']) {
                this.props.typeAheadChangeHandler(value);
              }
            }}
          />
      }
      </div>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExamsFormComponent)
