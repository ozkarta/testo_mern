import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import ExaminerTemplateComponent from '../template';
import ErrorAlertComponent from '../../../shared/components/error-alert';
import ModalWindowComponent from '../../../shared/components/modal-window';
import ExamsFormComponent from './exams.form';
import CopyClipboardComponent from '../../../shared/components/copy-clipboard';
import * as ExaminerExamActions from '../actions/exam.action';
import * as ExaminerExamineeActions from '../actions/examinee.actions';
import * as ExaminerExamOfferActions from '../actions/exam-offer.action';
import './index.css';
class ExaminerExamComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      exams: [],
      newExamData: null,
      isUpdateForm: false,
      showCreateNewExamWindowModal: false,
      showSendExamToExamineeWindowModal: false
    };

    this.createNewExam = this.createNewExam.bind(this);
    this.createExamModalWindowHandlers = this.createExamModalWindowHandlers.bind(this);
    this.examListComponentHandlers = this.examListComponentHandlers.bind(this);
    this.updateStateExamData = this.updateStateExamData.bind(this);
    this.sendExamToExamineeModalWindowHandlers = this.sendExamToExamineeModalWindowHandlers.bind(this);
    this.sendExamToExaminee = this.sendExamToExaminee.bind(this);
    this.addExamineeToMyRelationshipAndSendExam = this.addExamineeToMyRelationshipAndSendExam.bind(this);
  }

  componentDidMount() {
    let userId = this.state.user.user['_id'];
    this.requestExaminerExams(userId);
  }

  requestExaminerExams(examinerId) {
    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerExamActions.getExams(examinerId)
      .then(result => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.exams = result? (result.exams || []) : [];
        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  createNewExam() {
    let exam = Object.assign({});
    exam.test = null;
    exam.password = '';
    exam.title = '';
    exam.owner = this.state.user.user['_id'];
    // init exam here
    let stateCP = Object.assign({}, this.state);
    stateCP.isUpdateForm = false;
    stateCP.showCreateNewExamWindowModal = true;
    stateCP.newExamData = exam;
    this.setState(stateCP);
  }

  createExamModalWindowHandlers(isUpdateForm) {
    let handlers = {};
    handlers.modalExitClickHandler = (event) => {
      let stateCP = Object.assign({}, this.state);
      stateCP.showCreateNewExamWindowModal = false;
      stateCP.newExamData = null;
      this.setState(stateCP);
    };
    handlers.modalDiscardClickHandler = (event) => {
      let stateCP = Object.assign({}, this.state);
      stateCP.showCreateNewExamWindowModal = false;
      stateCP.newExamData = null;
      this.setState(stateCP);
    }

    if (isUpdateForm) {
      handlers.modalSubmitClickHandler = (event) => {
        event.preventDefault();

        let stateCP = Object.assign({}, this.state);
        stateCP.error = {
          msg: this.state.error?this.state.error.msg : '',
          visible: false
        };

        this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
        ExaminerExamActions.updateExam(this.state.newExamData)
          .then(exam => {
            stateCP.showCreateNewExamWindowModal = false;
            stateCP.newExamData = null;
            this.setState(stateCP);
            let userId = this.state.user.user['_id'];
            this.requestExaminerExams(userId);
          })
          .catch(error => {
            this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
            stateCP.error = {
              msg: error.msg,
              visible: true
            };
            this.setState(stateCP);
          });
      }
    } else {
      handlers.modalSubmitClickHandler = (event) => {
        event.preventDefault();

        let stateCP = Object.assign({}, this.state);
        stateCP.error = {
          msg: this.state.error?this.state.error.msg : '',
          visible: false
        };

        this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
        ExaminerExamActions.createExam(this.state.newExamData)
          .then(exam => {
            stateCP.showCreateNewExamWindowModal = false;
            stateCP.newExamData = null;
            this.setState(stateCP);
            let userId = this.state.user.user['_id'];
            this.requestExaminerExams(userId);
          })
          .catch(error => {
            this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
            stateCP.error = {
              msg: error.msg,
              visible: true
            };
            this.setState(stateCP);
          });
      }
    }

    return handlers;
  }

  examListComponentHandlers() {
    return {
      editClickHandler: (exam) => {
        let stateCP = Object.assign({}, this.state);
        stateCP.isUpdateForm = true;
        stateCP.showCreateNewExamWindowModal = true;
        stateCP.newExamData = exam;
        this.setState(stateCP);
      },
      deleteClickHandler: (exam) => {
        let userId = this.state.user.user['_id'];
        let stateCP = Object.assign({}, this.state);
        stateCP.error = {
          msg: this.state.error?this.state.error.msg : '',
          visible: false
        };

        this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
        ExaminerExamActions.deleteExam(exam['_id'])
          .then(result => {
            this.requestExaminerExams(userId);
          })
          .catch(error => {
            this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
            stateCP.error = {
              msg: error.msg,
              visible: true
            };
            this.setState(stateCP);
          });
      },
      sendToExamineeClickHandler: (exam) => {
        let stateCP = Object.assign({}, this.state);
        stateCP.showSendExamToExamineeWindowModal = true;
        stateCP.examToSend = exam;
        this.setState(stateCP);
      }
    }
  }

  updateStateExamData(updatedTestData) {
    let stateCP = Object.assign({}, this.state);
    stateCP.newTestData = updatedTestData;
    this.setState(stateCP);
  }

  sendExamToExamineeModalWindowHandlers() {
    let handlers = {};
    handlers.modalExitClickHandler = (event) => {
      let stateCP = Object.assign({}, this.state);
      stateCP.showSendExamToExamineeWindowModal = false;
      stateCP.examToSend = null;
      this.setState(stateCP);
    };
    handlers.modalDiscardClickHandler = (event) => {
      let stateCP = Object.assign({}, this.state);
      stateCP.showSendExamToExamineeWindowModal = false;
      stateCP.examToSend = null;
      this.setState(stateCP);
    }

    // handlers.modalSubmitClickHandler = (event) => {
    //   event.preventDefault();
    //   console.log('Sending...');
    // }

    return handlers;
  }

  sendExamToExaminee(examinee) {
    let exam = this.state.examToSend;
    let offer = Object.assign({});
    offer.from = this.state.user.user['_id'];
    offer.to = examinee? examinee['_id'] : '';
    offer.exam = exam? exam['_id'] : '';

    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerExamOfferActions.createExamOffer(offer)
      .then(result => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        if (result && result.exam) {
          stateCP.examToSend = result.exam;
        } else {
          stateCP.showSendExamToExamineeWindowModal = false;
          stateCP.examToSend = null;
        }
        this.setState(stateCP);
        this.requestExaminerExams(this.state.user.user['_id']);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  addExamineeToMyRelationshipAndSendExam(examinee) {
    // ExaminerExamOfferActions
    let exam = this.state.examToSend;
    let offer = Object.assign({});
    offer.from = this.state.user.user['_id'];
    offer.to = examinee;
    offer.exam = exam;

    console.dir(offer);


  }

  render() {
    return(
      <ExaminerTemplateComponent child={
          (
            <Fragment>
              <div>
                <br/><br/>
                <ModalWindowComponent {...this.createExamModalWindowHandlers(this.state.isUpdateForm)}
                                      show={this.state.showCreateNewExamWindowModal}
                                      submitButtonText={this.state.isUpdateForm? 'Update' : 'Create'}
                                      discardButtonText={this.state.isUpdateForm? 'discard' : 'Cancel'}
                                      modalTitle={'New Exam'}
                                      modalBody={(
                                        <Fragment>
                                          <ExamsFormComponent {...this.state} updateParentState={this.updateStateExamData}/>
                                        </Fragment>
                                      )}
                  />
                  <ModalWindowComponent {...this.sendExamToExamineeModalWindowHandlers(this.state.isUpdateForm)}
                                        show={this.state.showSendExamToExamineeWindowModal}
                                        submitButtonText={this.state.isUpdateForm? 'Send' : 'Send'}
                                        discardButtonText={this.state.isUpdateForm? 'Close' : 'Close'}
                                        modalTitle={'Send To Examinee'}
                                        modalBody={(
                                          <Fragment>
                                            <SendExamToExamineeComponent {...this.state}
                                              sendExamToExaminee={this.sendExamToExaminee}
                                              addExamineeToMyRelationshipAndSendExam={this.addExamineeToMyRelationshipAndSendExam}
                                                />
                                          </Fragment>
                                        )}
                    />


                <button type="button" onClick={this.createNewExam}>New Exam</button>
                <ExamListComponent exams={this.state.exams} {...(this.examListComponentHandlers())} {...this.state}/>
              </div>
            </Fragment>
          )
      }/>
    );
  }
}

class ExamListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  render() {
    return(
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Create Date</th>
            <th scope="col">Title</th>
            <th scope="col">Test Type</th>
            <th scope="col">Partial Question QTY</th>
            <th scope="col">Test Items</th>
            <th scope="col">Copy to Clipboard</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {
            this.props && this.props.exams && this.props.exams.map((exam, index) => {


              return (
                <tr key={index}>
                  <th scope="row">{index + 1}</th>
                  <td>{exam.createdAt}</td>
                  <td>{exam.title}</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>
                    <CopyClipboardComponent clipBoardText={`${document.location.origin}/examinee/exam/${exam['_id']}`} index={index}/>
                  </td>
                  <td>
                    <div className="row">
                      <button type="button" onClick={(event) => {
                          event.preventDefault();
                          this.props.editClickHandler(exam);
                        }}>
                        Edit
                      </button>
                      <button type="button" onClick={(event) => {
                          event.preventDefault();
                          this.props.deleteClickHandler(exam);
                        }}>
                        Delete
                      </button>

                      <button type="button" onClick={(event) => {
                          event.preventDefault();
                          this.props.sendToExamineeClickHandler(exam);
                        }}>
                        Send
                      </button>
                    </div>
                  </td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
    );
  }
}

class SendExamToExamineeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      myExamineesSearchTerm: '',
      globalExamineesSearchTerm: 'ozbegi3@gmail.com',
      globalExamineesSearchDone: false,
      globalExamineeSearchResult: null
    }
    this.loadExamineesInRelationship = this.loadExamineesInRelationship.bind(this);
    this.defaultChangeHandler = this.defaultChangeHandler.bind(this);
    this.searchExamineeByEmail = this.searchExamineeByEmail.bind(this);
    this.isValidEmail = this.isValidEmail.bind(this);
  }

  componentDidMount() {
    this.loadExamineesInRelationship();
  }

  loadExamineesInRelationship() {
    let userId = this.state.user.user['_id'];
    let stateCP = Object.assign({}, this.state);

    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerExamineeActions.getExamineeRelationships(userId)
      .then(result => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.myExaminees = result.examinees;
        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  defaultChangeHandler(value, field) {
    let stateCP = Object.assign({}, this.state);
    stateCP[field] = value;
    this.setState(stateCP);
  }

  searchExamineeByEmail(event) {
    event.preventDefault();
    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };
    stateCP.globalExamineesSearchDone = true;
    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
    ExaminerExamineeActions.getExamineeByEmail(this.state.globalExamineesSearchTerm)
      .then(result => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.globalExamineeSearchResult = result? result.examinee : null;
        this.setState(stateCP);
      })
      .catch(error => {
        this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
        stateCP.error = {
          msg: error.msg,
          visible: true
        };
        this.setState(stateCP);
      });
  }

  isValidEmail(email) {
    return true;
  }

  render() {
    console.log('render');
    return(
      <Fragment>
          <ErrorAlertComponent {...this.state.error}/>
          <h3>Select from Your Contacts</h3>
          <div>
            <input className="form-control" id="myInput" type="text" placeholder="Search.."
                value={this.state.myExamineesSearchTerm} onChange={(event) => {this.defaultChangeHandler(event.target.value, 'myExamineesSearchTerm')}}/>
             <br/>
             <table className="table table-bordered table-striped">
               <thead>
                 <tr>
                   <th>Firstname</th>
                   <th>Lastname</th>
                   <th>Email</th>
                   <th>Action</th>
                 </tr>
               </thead>
               <tbody id="myTable">
                 {
                   this.state.myExaminees && this.state.myExaminees.map((examinee, index) => {
                     let disableSendButton = false;
                     this.props.examToSend && this.props.examToSend.offers && this.props.examToSend.offers.forEach(offer => {
                       if (!offer) {
                         return;
                       }
                       if (offer && offer['to'] === examinee['_id']) {
                         disableSendButton = true;
                       }
                     })

                     if ( `${examinee.firstName} ${examinee.lastName}`.match(new RegExp(`^${this.state.myExamineesSearchTerm}.*`, 'i')) ||
                          `${examinee.firstName} ${examinee.lastName}`.match(new RegExp(`.*${this.state.myExamineesSearchTerm}$`, 'i')) ||
                          `${examinee.firstName} ${examinee.lastName}`.match(new RegExp(`.*${this.state.myExamineesSearchTerm}.*`, 'i')) ||

                          examinee.email.match(new RegExp(`^${this.state.myExamineesSearchTerm}.*`, 'i')) ||
                          examinee.email.match(new RegExp(`.*${this.state.myExamineesSearchTerm}$`, 'i')) ||
                          examinee.email.match(new RegExp(`.*${this.state.myExamineesSearchTerm}.*`, 'i'))

                        ) {
                       return(
                         <tr key={index}>
                           <td>{index+1}</td>
                           <td>{`${examinee['firstName']} ${examinee['lastName']}`}</td>
                           <td>{examinee['email']}</td>
                           <td>
                             <button type="button" disabled={disableSendButton} onClick={(event) => {this.props.sendExamToExaminee(examinee)}}>Send</button>
                           </td>
                         </tr>
                       )
                     }

                     return null;
                   })
                 }

               </tbody>
             </table>
          </div>

          <h3>Search By Email address</h3>
            <div className="input-group mb-3">
              <input type="text" className="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2"
                  value={this.state.globalExamineesSearchTerm} onChange={(event) => {this.defaultChangeHandler(event.target.value, 'globalExamineesSearchTerm')}}/>

              <div className="input-group-append">
                <span className="input-group-text" id="basic-addon2">
                  <button type="button" onClick={(event) => {this.searchExamineeByEmail(event)}}>Search</button>
                </span>
              </div>
            </div>

            <br/>
            {
              this.state.globalExamineeSearchResult &&
              <GlobalExamineeSearchResultViewComponent
                item={this.state.globalExamineeSearchResult}
                addExamineeToMyRelationshipAndSendExam={this.props.addExamineeToMyRelationshipAndSendExam}/>
            }
            {
              !this.state.globalExamineeSearchResult && this.state.globalExamineesSearchDone && this.state.globalExamineesSearchTerm && this.isValidEmail(this.state.globalExamineesSearchTerm) &&
              <AskForCreateNewExamineeComponent {...this.state}/>
            }
      </Fragment>
    );
  }
}

class GlobalExamineeSearchResultViewComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return(
      <table className="table table-bordered table-striped">
        <thead>
          <tr>

            <th>Name</th>
            <th>Email</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="myTable">
          <tr>

            <td>{`${this.props.item['firstName']} ${this.props.item['lastName']}`}</td>
            <td>{this.props.item['email']}</td>
            <td>
              <button type="button" onClick={() => this.props.addExamineeToMyRelationshipAndSendExam(this.props.item)}>Send</button>
            </td>
          </tr>
        </tbody>
      </table>
    );
  }
}

class AskForCreateNewExamineeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return(
      <Fragment>
        <h3>No result found for yoour Search.</h3>
        <h3>
          <button type="button" onClick={(event) => {this.props.history.push(`/examiner/examinees/new-examinee/${this.props.globalExamineesSearchTerm}`)}}>Create New Examinee?</button>
        </h3>
      </Fragment>
    );
  }
}

export default connect(
    (state) => {
        return {
          user: state.user
        };
    }
)(ExaminerExamComponent)
