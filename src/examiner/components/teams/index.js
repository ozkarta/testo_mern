import React, {Component} from 'react';
import ExaminerTemplateComponent from '../template';

import './index.css';
export default class ExaminerTeamsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    };
  }

  render() {
    return(
      <ExaminerTemplateComponent child={(<h1>აქ დააჯგუფებს იუზერებს, შექმნის ჯგუფებს, გაუგზავნის  ტესტის ლინკს ერთად ყველას, ნახავს შედეგებს</h1>)}/>
    );
  }
}
