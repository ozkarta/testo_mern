import React, {Component, Fragment} from 'react';
import NavbarComponent from '../navbar';
import ExaminerSidebar from '../sidebar';
import './index.css';

export default class ExaminerTemplateComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return(
      <Fragment>
        <div className="container-fluid">
          <div className="row">
            <NavbarComponent/>
            <ExaminerSidebar/>

            <main className="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
              <br/>
              {this.props.child}
            </main>

          </div>
        </div>
      </Fragment>
    );
  }
}
