import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import LogOutComponent from '../../../shared/components/logout';

class NavbarComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <Link className="nav-link" to={'/'}>TestO</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <LogOutComponent/>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default connect(
    (state) => {
        return {
            user: state.user
        };
    }
)(NavbarComponent)
