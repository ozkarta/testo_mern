import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import App from './App';
import store from './shared/redux-store';
import BusyIndicator from './shared/components/busy-indicator';

import './index.css';
//import registerServiceWorker from './registerServiceWorker';
function render() {
  ReactDOM.render(
    <Provider store={store}>
      <React.Fragment>
        <App/>
        <BusyIndicator color="#000000" type="spin"/>
      </React.Fragment>
    </Provider>
    , document.getElementById('root'));
}

//registerServiceWorker();
store.subscribe(render);
render();
