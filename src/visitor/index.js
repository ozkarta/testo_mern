import React, {Component} from 'react';
import VisitorLandingComponent from './components/landing';

export default class VisitorComponents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    return (
      <React.Fragment>
        <VisitorLandingComponent/>
      </React.Fragment>
    );
  }
}
