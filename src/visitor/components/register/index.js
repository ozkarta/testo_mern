import React, {Component} from 'react';
import HeaderV2Component from '../navbar';
import {connect} from 'react-redux';
import * as RegisterActions from './register.action';

class RegisterComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      user: {
        firstName: 'oz',
        lastName: 'kart',
        email: 'ozbegi1@gmail.com',
        password: '12qwert12',
        role: 'examiner'
      }
    }

    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.formSubmitEventHandler = this.formSubmitEventHandler.bind(this);
  }

  inputChangeHandler(value, field) {
    let stateCP = Object.assign({}, this.state);
    stateCP.user[field] =  value;
    this.setState(stateCP);
  }

  formSubmitEventHandler(event) {
    event.preventDefault();
    let stateCP = Object.assign({}, this.state);
    stateCP.error = {
      msg: this.state.error?this.state.error.msg : '',
      visible: false
    };

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
        RegisterActions.register(this.state.user)
            .then(
                action => {
                    this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
                    this.props.dispatch(action);
                }
            )
            .catch(error => {
                this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
                stateCP.error = {
                  msg: error.msg,
                  visible: true
                };
                this.setState(stateCP);
            });
  }

  render() {
    return(
      <React.Fragment>
        <HeaderV2Component/>
        <br/>
        <br/>
        <br/>

        <ErrorAlertComponent {...this.state.error}/>

        <form onSubmit={this.formSubmitEventHandler}>
          <div className="form-group">
            <label htmlFor="fName">First Name:</label>
            <input type="text" className="form-control" id="fName"
              value={this.state.user.firstName} onChange={(event) => {this.inputChangeHandler(event.target.value, 'firstName')}}/>
          </div>

          <div className="form-group">
            <label htmlFor="lName">Last Name:</label>
            <input type="text" className="form-control" id="lName"
              value={this.state.user.lastName} onChange={(event) => {this.inputChangeHandler(event.target.value, 'lastName')}}/>
          </div>

          <div className="form-group">
            <label htmlFor="email">Email address:</label>
            <input type="email" className="form-control" id="email"
              value={this.state.user.email} onChange={(event) => {this.inputChangeHandler(event.target.value, 'email')}}/>
          </div>

          <div className="form-group">
            <label htmlFor="pwd">Password:</label>
            <input type="password" className="form-control" id="pwd"
              value={this.state.user.password} onChange={(event) => {this.inputChangeHandler(event.target.value, 'password')}}/>
          </div>

          <div className="form-group">
            <label htmlFor="rpwd">Repeat Password:</label>
            <input type="password" className="form-control" id="rpwd"/>
          </div>


          <button type="submit" className="btn btn-primary">Register</button>
        </form>
      </React.Fragment>
    );
  }
}

class ErrorAlertComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  render() {
    if (this.props && this.props.visible) {
      return(
        <div className="alert alert-danger">
          <strong>Danger!</strong> {this.props.msg}
        </div>
      );
    }

    return null;
  }
}

export default connect(
    (state) => {
        return {
            user: state.user
        };
    }
)(RegisterComponent);
