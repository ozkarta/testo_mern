import React, {Component} from 'react';
import HeaderV2Component from '../navbar';
import {connect} from 'react-redux';
import ErrorAlertComponent from '../../../shared/components/error-alert';
import * as LoginActions from './login.action';

class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      user: {
        email: 'ozbegi1@gmail.com',
        password: '12qwert12'
      }
    }

    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.formSubmitEventHandler = this.formSubmitEventHandler.bind(this);
  }

  inputChangeHandler(value, field) {
    let stateCP = Object.assign({}, this.state);
    stateCP.user[field] =  value;
    this.setState(stateCP);
  }

  formSubmitEventHandler(event) {
    event.preventDefault();

    this.state.dispatch({type: 'BUSY_INDICATOR', busy: true});
        LoginActions.authenticate(this.state.user)
            .then(
                action => {
                    this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
                    this.props.dispatch(action);
                }
            )
            .catch(error => {
                this.state.dispatch({type: 'BUSY_INDICATOR', busy: false});
                this.setState({error: {
                  msg: error.msg,
                  visible: true
                }});
            });
  }

  render() {
    return(
      <React.Fragment>
        <HeaderV2Component/>
        <br/>
        <br/>
        <br/>

      <ErrorAlertComponent {...this.state.error}/>

        <form onSubmit={this.formSubmitEventHandler}>
          <div className="form-group">
            <label htmlFor="email">Email address:</label>
            <input type="email" className="form-control" id="email"
              value={this.state.user.email}
              onChange={(event) => {this.inputChangeHandler(event.target.value, 'email')}}/>
          </div>
          <div className="form-group">
            <label htmlFor="pwd">Password:</label>
            <input type="password" className="form-control" id="pwd"
              value={this.state.user.password}
              onChange={(event) => {this.inputChangeHandler(event.target.value, 'password')}}/>
          </div>
          <div className="form-group form-check">
            <label className="form-check-label">
              <input className="form-check-input" type="checkbox"/> Remember me
            </label>
          </div>
          <button type="submit" className="btn btn-primary">Log In</button>
        </form>
      </React.Fragment>
    );
  }
}

export default connect(
    (state) => {
        return {
            user: state.user
        };
    }
)(LoginComponent)
